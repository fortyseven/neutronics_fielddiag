﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tests
{
    class Program
    {
        static void Main( string[] args )
        {
            FieldDiagnostic.Diags.Results res = new FieldDiagnostic.Diags.Results();
            try {
                res.Raw = "000.0 000.0 000.0 000.0 100.0 00005 00023";
                Console.WriteLine( res.ToString() );
            }
            catch ( Exception e ) {
                Console.WriteLine( "Error parsing analysis data: " + e.Message );
            }
            try {
                res.Raw = "000.0 000.0 000.0 000.0 100.0 3 0005 0023";
                Console.WriteLine( res.ToString() );
            }
            catch ( Exception e ) {
                Console.WriteLine( "Error parsing analysis data: " + e.Message );
            }
            try {
                res.Raw = "Wank";
                Console.WriteLine( res.ToString() );
            }
            catch ( Exception e ) {
                Console.WriteLine( "Error parsing analysis data: " + e.Message );
            }
            Console.ReadKey();
        }
    }
}
