; Script generated by the Inno Script Studio Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "R1234yf Field Diagnostic Tool"
#define MyAppVersion "3.0.1"
#define MyAppExeName "Identifier Diagnostic.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{CE0E7EEF-C119-4C8D-9C24-55103E1212F4}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
AllowNoIcons=yes
OutputDir=..\Releases
OutputBaseFilename=field-diagnostic-setup-{#MyAppVersion}
;Compression=lzma
;SolidCompression=yes
ShowLanguageDialog=auto
MinVersion=0,5.01

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; 
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; OnlyBelowVersion: 0,6.1

[Files]
Source: "..\FieldDiagnostic\bin\Release\Identifier Diagnostic.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "driver\dpinst-amd64.exe"; DestDir: "{app}\driver"; Flags: ignoreversion deleteafterinstall
Source: "driver\dpinst-x86.exe"; DestDir: "{app}\driver"; Flags: ignoreversion deleteafterinstall
Source: "driver\492-303.*"; DestDir: "{app}\driver"; Flags: ignoreversion deleteafterinstall; AfterInstall: InstallDriver

; Flags: ignoreversion deleteafterinstall
; Check: CheckDriver; AfterInstall: InstallDriver;
Source: "vendor\dotnetfx35.exe"; DestDir: "{app}"; Flags: ignoreversion deleteafterinstall; Check: CheckDotNet; AfterInstall: InstallDotNet
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: quicklaunchicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
;Filename: "{sys}\rundll32.exe"; Parameters: "SETUPAPI.DLL,InstallHinfSection DriverInstall.nt 128 {app}\driver\492-200.inf"; WorkingDir: {app}\driver; Flags: 32bit;
;Filename: "{sys}\rundll32.exe"; Parameters: "SETUPAPI.DLL,InstallHinfSection DriverInstall.NTamd64 128 {app}\driver\492-200.inf"; WorkingDir: {app}\driver; Flags: 64bit;

[ThirdParty]
UseRelativePaths=True

[Code]
{-----------------------------------------------------------------------}
function IsDotNetDetected(version: string; service: cardinal): boolean;
// Indicates whether the specified version and service 
// pack of the .NET Framework is installed.
//
// version -- Specify one of these strings for the required .NET 
//            Framework version:
//    'v1.1.4322'     .NET Framework 1.1
//    'v2.0.50727'    .NET Framework 2.0
//    'v3.0'          .NET Framework 3.0
//    'v3.5'          .NET Framework 3.5
//    'v4\Client'     .NET Framework 4.0 Client Profile
//    'v4\Full'       .NET Framework 4.0 Full Installation
//    'v4.5'          .NET Framework 4.5
//
// service -- Specify any non-negative integer for the required 
//            service pack level:
//    0               No service packs required
//    1, 2, etc.      Service pack 1, 2, etc. required
// 
// CREDIT: [ http://www.kynosarges.de/DotNetVersion.html ]
var
    key             :string;
    install, 
    release, 
    serviceCount    :cardinal;
    check45, 
    success         :boolean;
begin
    // .NET 4.5 installs as update to .NET 4.0 Full
    if version = 'v4.5' then 
    begin
        version := 'v4\Full';
        check45 := true;
    end else
        check45 := false;

    // installation key group for all .NET versions
    key := 'SOFTWARE\Microsoft\NET Framework Setup\NDP\' + version;

    // .NET 3.0 uses value InstallSuccess in subkey Setup
    if Pos('v3.0', version) = 1 then 
    begin
        success := RegQueryDWordValue(HKLM, key + '\Setup', 
                                                'InstallSuccess', install);
    end else begin
        success := RegQueryDWordValue(HKLM, key, 
                                                'Install', install);
    end;

    // .NET 4.0/4.5 uses value Servicing instead of SP
    if Pos('v4', version) = 1 then 
    begin
        success := success and RegQueryDWordValue(HKLM, key, 
                                                'Servicing', serviceCount);
    end else begin
        success := success and RegQueryDWordValue(HKLM, key, 
                                                'SP', serviceCount);
    end;

    // .NET 4.5 uses additional value Release
    if check45 then 
    begin
        success := success and RegQueryDWordValue(HKLM, key, 
                                                'Release', release);
        success := success and (release >= 378389);
    end;

    result := success and (install = 1) and (serviceCount >= service);
end;

{-----------------------------------------------------------------------}
Procedure InstallDotNet();
var
    lastcaption, 
    path        :string;
    kode        :integer;
Begin
    path:= ExpandConstant('{app}\dotnetfx35.exe');
    lastcaption:=WizardForm.StatusLabel.Caption;
    WizardForm.StatusLabel.Caption := 'Installing required .NET Framework 3.5 SP1... (this may take a few minutes)';
    Exec( path, '/Q /Norestart', '', SW_hide, ewWaitUntilTerminated, kode );
    WizardForm.StatusLabel.Caption := lastcaption;
End;

{-----------------------------------------------------------------------}
function CheckDotNet():Boolean;
begin
    Result := False;
    if not IsDotNetDetected('v3.5', 0) then Result := True;
end;

{-----------------------------------------------------------------------}
function CheckDriver():Boolean;
begin
    // Here is where we could check for the existence of the driver, but
    // it's not really necessary except to avoid "unsigned" warnings
    // on install, but... we'll come back to that if it's necessary.

    Result := True;
end;

{-----------------------------------------------------------------------}
Procedure InstallDriver();
var
    ResultCode   :integer;
    prev_caption :string;
begin
    prev_caption := WizardForm.StatusLabel.Caption;
    WizardForm.StatusLabel.Caption := 'Installing identifier device driver...';
    If IsWin64 then begin
        Exec(ExpandConstant('{app}\driver\dpinst-amd64.exe'), '/lm /sw', //se
                                ExpandConstant('{app}\driver'), 
                                SW_SHOW, ewWaitUntilTerminated, ResultCode);
    end else begin
        Exec(ExpandConstant('{app}\driver\dpinst-x86.exe'), '/lm /sw', 
                                ExpandConstant('{app}\driver'), 
                                SW_SHOW, ewWaitUntilTerminated, ResultCode);
    end;
    WizardForm.StatusLabel.Caption := prev_caption;
end;

function InitializeSetup():Boolean;
var
    Version: TWindowsVersion;
begin
    GetWindowsVersionEx(Version);

    // On Windows XP, check for SP2
    if Version.NTPlatform and
        (Version.Major = 5) and
        (Version.Minor = 1) and
        (Version.ServicePackMajor < 2) then
    begin
        SuppressibleMsgBox('When running on Windows XP, Service Pack 2 is required.', mbCriticalError, MB_OK, MB_OK);
        Result := False;
        Exit;
    end;

    Result := True;
end;
