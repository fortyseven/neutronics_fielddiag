﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using FieldDiagnostic.Device;
using FieldDiagnostic.Diags;

namespace FieldDiagnostic
{
    public class AppState
    {
        private const string RESULTCODE_VDA_ALL_AIR = "00005";
        private const string RESULTCODE_VDA_ALL_GAS = "00000";
        private const string RESULTCODE_VDA_ALL_ERR = "99999";


        public bool WasSaved { get; private set; }

        public string TechName { get; set; }
        public string SerNo { get; set; }
        public string Notes { get; set; }

        public bool PrelimPoweredUp { get; set; }
        public bool PrelimInspectFilter { get; set; }
        public bool PrelimInspectLine { get; set; }
        public bool PrelimReplacedBrass { get; set; }

        public float FlowMeterReading { get; set; }

        public AnalyzerDevice Device { get; set; }

        public Results ResultsGasAnal;
        public Results ResultsAirAnal;

        public bool IdentifierDetectionFailed { get; set; }
        public string IdentifierDetectionFailureMessage { get; set; }
        /*******************************************************/
        public AppState()
        {
            ResultsGasAnal = new Results();
            ResultsAirAnal = new Results();
            Reset();
        }

        /*******************************************************/
        private void Reset()
        {
            WasSaved = false;

            TechName = "";
            SerNo = "";
            Notes = "";

            PrelimPoweredUp = false;
            PrelimInspectFilter = false;
            PrelimInspectLine = false;
            PrelimReplacedBrass = false;

            ResultsAirAnal.Clear();
            ResultsGasAnal.Clear();

            Device = null;
            IdentifierDetectionFailed = false;
            IdentifierDetectionFailureMessage = null;
        }

        /*******************************************************/
        public void Save()
        {
            string serial = "";

            if ( Device != null ) {
                // Try to use the device serial
                if ( Device.SerialNumber.Length > 0 ) {
                    serial = "_" + Device.SerialNumber;
                }
            }
            // Fall back to user provided serial
            else if ( SerNo.Length > 0 ) {
                serial = "_" + SerNo;
            }

            string timestamp = DateTime.Now.ToString( "yyyy-MM-ddTHHmmss" ); // ISO 8601. Roughly.

            // Worst case, it'll just be empty.

            SaveFileDialog sfd = new SaveFileDialog {
                Filter = "Text files (*.txt)|*.txt|All files(*.*)|*.*",
                FileName = "results" + serial + "_" + timestamp + ".txt",
                FilterIndex = 1,
                RestoreDirectory = true
            };

            if ( sfd.ShowDialog() != DialogResult.OK )
                return;

            if ( MainForm.State.IdentifierDetectionFailed ) {
                SaveNotFoundStateAsTextTo( sfd.FileName );
            }
            else {
                SaveResultsAsTextTo( sfd.FileName );
            }

            WasSaved = true;
        }

        /*******************************************************/
        private const string HR = "---------------------------------------------";
        private void SaveResultsAsTextTo( string path )
        {
            StringBuilder output = new StringBuilder();

            output.AppendLine( HR );
            output.AppendLine( "R1234yf ID SAE Field Diagnostics Report " );
            output.AppendLine( HR );
            output.AppendLine( "TESTING DATE:           " + DateTime.Now.ToLocalTime() );
            output.AppendLine( HR );
            output.AppendLine( "TECHNICIAN:             " + MainForm.State.TechName );
            output.AppendLine( "REPORTED SERIAL:        " + MainForm.State.SerNo );
            output.AppendLine();
            output.AppendLine( "LED LIT?                " + MainForm.State.PrelimPoweredUp );
            output.AppendLine( "FILTER CLEAN?           " + MainForm.State.PrelimInspectFilter );
            output.AppendLine( "SAMPLE LINE CLEAN?      " + MainForm.State.PrelimInspectLine );
            output.AppendLine( "FITTINGS & FILTER RPL?  " + MainForm.State.PrelimReplacedBrass );
            output.AppendLine( HR );

            if ( MainForm.State.Notes.Length > 0 ) {
                output.AppendLine( "TECHNICIAN NOTES:       " + MainForm.State.Notes.Replace( "\n", " " ) );
                output.AppendLine( HR );
            }

            output.AppendLine( "COM PORT:               " + MainForm.State.Device.PortName );
            output.AppendLine( "DEVICE TYPE:            " + MainForm.State.Device.DeviceType );
            output.AppendLine( "EEPROM SERIAL NUMBER:   " + MainForm.State.Device.SerialNumber );
            output.AppendLine( "SOFTWARE VERSION:       " + MainForm.State.Device.SoftwareRevision );
            output.AppendLine( "FIRMWARE BUILD:         " + MainForm.State.Device.FirmwareBuild );

            if ( HasErrors() )
                output.AppendLine( "UPDATED TO LATEST?      True" ); // Not like it can be anything else...

            output.AppendLine( HR );
            output.AppendLine( "LAST FIVE READINGS:" );
            foreach ( string result in Device.LastResults ) {
                output.AppendLine( "                    --  " + result );
            }
            output.AppendLine( HR );
            if ( Device.DeviceType == AnalyzerDevice.Type.VDA ) {
                output.AppendLine( "GAS ANALYSIS EXPECTING: " + RESULTCODE_VDA_ALL_GAS + " xxxxx" );
            }
            else {
                output.AppendLine( "GAS ANALYSIS EXPECTING: xxxxx 100.0 xxxxx xxxxx 000.0 00000 xxxxx" );
            }
            output.AppendLine( "GAS ANALYSIS RESULT:    " + MainForm.State.ResultsGasAnal.Raw );
            output.AppendLine( "FLOW METER READING:     " + MainForm.State.FlowMeterReading );

            output.AppendLine( HR );
            if ( Device.DeviceType == AnalyzerDevice.Type.VDA ) {
                output.AppendLine( "AIR ANALYSIS EXPECTING: " + RESULTCODE_VDA_ALL_AIR + " xxxxx" );
            }
            else {
                output.AppendLine( "AIR ANALYSIS EXPECTING: xxxxx 000.0 xxxxx xxxxx 100.0 00005 xxxxx" );
            }
            output.AppendLine( "AIR ANALYSIS RESULT:    " + MainForm.State.ResultsAirAnal.Raw );
            output.AppendLine( HR );
            output.AppendLine();
            output.AppendLine( HasErrors() ? "Errors detected." : "No errors found." );

            File.WriteAllText( path, output.ToString() );
            WasSaved = true;
        }

        /*******************************************************/
        private void SaveNotFoundStateAsTextTo( string path )
        {
            StringBuilder output = new StringBuilder();

            output.AppendLine( HR );
            output.AppendLine( "R1234yf ID SAE Field Diagnostics Report " );
            output.AppendLine( HR );
            output.AppendLine( "TESTING DATE:           " + DateTime.Now.ToLocalTime() );
            output.AppendLine( HR );
            output.AppendLine( "TECHNICIAN:             " + MainForm.State.TechName );
            output.AppendLine( "REPORTED SERIAL:        " + MainForm.State.SerNo );
            output.AppendLine();
            output.AppendLine( "LED LIT?                " + MainForm.State.PrelimPoweredUp );
            output.AppendLine( "FILTER CLEAN?           " + MainForm.State.PrelimInspectFilter );
            output.AppendLine( "SAMPLE LINE CLEAN?      " + MainForm.State.PrelimInspectLine );
            output.AppendLine( "FITTINGS & FILTER RPL?  " + MainForm.State.PrelimReplacedBrass );
            output.AppendLine( HR );

            if ( MainForm.State.Notes.Length > 0 ) {
                output.AppendLine( "TECHNICIAN NOTES:       " + MainForm.State.Notes.Replace( "\n", " " ) );
                output.AppendLine( HR );
            }

            output.AppendLine( "*** DID NOT FIND AN IDENTIFIER ***" );
            output.AppendLine();
            output.AppendLine( MainForm.State.IdentifierDetectionFailureMessage );
            output.AppendLine();

            File.WriteAllText( path, output.ToString() );
            WasSaved = true;
        }

        /*******************************************************/
        public override string ToString()
        {
            return "Port: " + Device.PortName +
                   "\nName : " + Device.DeviceName +
                   "\nDSrNo: " + Device.SerialNumber +
                   "\nRevis: " + Device.SoftwareRevision +
                   "\nBuild: " + Device.FirmwareBuild +
                   "\nTechN: " + TechName +
                   "\nSerNo: " + SerNo +
                   "\nNotes: " + Notes +
                   "\nInspF: " + PrelimInspectFilter +
                   "\nInspL: " + PrelimInspectLine +
                   "\nPowUp: " + PrelimPoweredUp +
                   "\nReBra: " + PrelimReplacedBrass +
                   "\nFlowM: " + FlowMeterReading +
                   "\nGAnal: " + ResultsGasAnal +
                   "\nAanal: " + ResultsAirAnal
                //+
                //"\nGPrev0: " + Device.LastResults[ 0 ] +
                //"\nGPrev1: " + Device.LastResults[ 1 ] +
                //"\nGPrev2: " + Device.LastResults[ 2 ] +
                //"\nGPrev3: " + Device.LastResults[ 3 ] +
                //"\nGPrev4: " + Device.LastResults[ 4 ]
                ;
        }

        /*******************************************************/
        public bool HasErrors()
        {
            bool result = false;

            Debug.Assert( ResultsAirAnal.Raw != null && ResultsGasAnal.Raw != null );

            switch ( Device.DeviceType ) {
                case AnalyzerDevice.Type.VDA:
                    Debug.WriteLine( "a = " + ResultsAirAnal.Raw + "\ng = " + ResultsGasAnal.Raw );
                    if ( ( ResultsAirAnal.ResultCode != RESULTCODE_VDA_ALL_AIR ) ||
                         ( ResultsGasAnal.ResultCode != RESULTCODE_VDA_ALL_GAS ) ) {
                        result = true;
                    }
                    break;
                case AnalyzerDevice.Type.SAE:
                    // We're expecting excessive air to be reported here
                    if ( ResultsAirAnal.ResultCode != Results.RESULT_CODE_SAE_EXCESSIVE_AIR ) {
                        //ErrorMessage = "Excessive air";
                        result = true;
                    }

                    try {
                        if ( Double.Parse( ResultsGasAnal.R1234yf, NumberStyles.Float ) < 99.00f ) {
                            //ErrorMessage = "R1234yf < 99.00";
                            result = true;
                        }
                    }
                    catch ( Exception ) {
                        result = true;
                    }
                    break;
            }

            return result;
        }

        /*******************************************************/
        internal void SetDetectionFailed( string supplemental_error_msg )
        {
            IdentifierDetectionFailed = true;
            IdentifierDetectionFailureMessage = supplemental_error_msg;
        }
    }
}
