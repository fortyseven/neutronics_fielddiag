﻿namespace FieldDiagnostic.Pages
{
    /*
     * Page 1 - Welcome
     */

    class WP_Page01_Welcome : IWizardPage
    {
        public WP_Page01_Welcome( WizardController wizard )
            : base( wizard )
        {
        }

        public override void OnPageEnter()
        {
            _wizard.LockPrev();
            _wizard.UnlockNext();
        }
    }
}
