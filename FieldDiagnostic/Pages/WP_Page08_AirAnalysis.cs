﻿using System;
using System.Windows.Forms;
using FieldDiagnostic.Device;
using FieldDiagnostic.Pages.UI;

namespace FieldDiagnostic.Pages
{
    /*
     * Page 8 - Prepare for Air Analysis
     */

    class WP_Page08_AirAnalysis : IWizardPage
    {
        private const int BUSY_WAIT_SECS = 100;

        public WP_Page08_AirAnalysis( WizardController wizard )
            : base( wizard )
        {
        }

        private void InitThread()
        {
            AnalyzerDevice device =  MainForm.State.Device;

            BusyForm.Show( "Performing recalibration and air analysis...please wait.", BUSY_WAIT_SECS );

            try {
                device.CmdSystemCheck();
                device.CmdDoCalibration();
                string result = device.CmdDoAnalysis();
                //var result = "000.0 000.0 000.0 000.0 100.0 00005 00023";

                if ( result == null )
                    throw new Exception( "Device returned unknown response." );

                MainForm.State.ResultsAirAnal.Raw = result;

                BusyForm.Dismiss();
                _wizard.NextForce();
            }
            catch ( Exception e ) {
                BusyForm.Dismiss();
                MessageBox.Show( "There was a problem performing air analysis.\n" + e.Message, "Analysis Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
            finally {
                _wizard.UnlockNext();
            }
        }
/*
        private void OldInitThread()
        {
            AnalyzerDevice device =  MainForm.State.Device;
            BusyForm.Show( "Performing recalibration and air analysis...please wait." );
            try {
                // Device will need to be recalibrated, apparently.
                device.CmdSystemCheck();
                if ( !device.CmdDoCalibration() ) {
                    MessageBox.Show( "There was a problem recalibrating for air analysis. Check the log for details.", "Failure",
                                                                                MessageBoxButtons.OK, MessageBoxIcon.Asterisk );
                    //FIXME: check error handling
                    _wizard.UnlockNext();
                    return;
                }
                string result = device.CmdDoAnalysis();
                if ( result != null ) {
                    //MainForm.State.Log.Write( "Air analysis completed: " + result );
                    try {
                        MainForm.State.ResultsAirAnal.Raw = result;
                        // BEST OF ALL POSSIBLE RESULTS 
                        //_wizard.NextForce();
                    }
                    catch ( Exception e ) {
                        //MainForm.State.Log.Write( "Problem parsing air analysis repsponse: " + e.Message );
                        MessageBox.Show( "There was a problem reading air analysis results.\n" + e.Message, "Failure",
                                                                MessageBoxButtons.OK, MessageBoxIcon.Asterisk );
                        _wizard.UnlockNext();
                    }
                }
                else {
                    MessageBox.Show( "There was a problem performing air analysis. Check the log for details.", "Failure",
                                                            MessageBoxButtons.OK, MessageBoxIcon.Asterisk );
                    _wizard.UnlockNext();
                }
            }
            catch ( Exception e ) {
                //MainForm.State.Log.Write( "Device error: " + e.Message );
                MessageBox.Show( e.Message, "Device Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
            finally {
                BusyForm.Dismiss();
            }

            _wizard.NextForce();
        }
*/
        public override void OnPageEnter()
        {
            _wizard.UnlockPrev();
            _wizard.UnlockNext();
        }

        public override bool OnPageExit()
        {
            _wizard.LockPrev();
            _wizard.LockNext();
            Run( InitThread );
            return false;
        }
    }
}
