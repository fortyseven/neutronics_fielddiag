﻿using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace FieldDiagnostic.Pages
{
    /*
     * Page 10 - Errors and Results Document
     */

    class WP_Page10_Finish : IWizardPage
    {
        public WP_Page10_Finish( WizardController wizard )
            : base( wizard )
        {
        }

        private CheckBox _cb_latest_firmware;
        private bool DisableValidation { get; set; }

        private void MainLoop()
        {
            if ( DisableValidation )
                return;

            while ( Running ) {
                if ( _cb_latest_firmware.Checked ) {
                    _wizard.UnlockNext();
                }
                else {
                    _wizard.LockNext();
                }

                Thread.Sleep( 100 );
                Application.DoEvents();
            }
        }

        public override void OnPageEnter()
        {
            //#if DEBUG
            //            DEBUG_FakeData();
            //#endif

            _cb_latest_firmware = (CheckBox)_parent_form
                                            .Controls[ "tabControl1" ]
                                            .Controls[ "tabPage10" ]
                                            .Controls[ "panel1" ]
                                            .Controls[ "cbLatestFirmware" ];

            _parent_form.Controls[ "btnNext" ].SynchronizedInvoke( () =>
                    _parent_form.Controls[ "btnNext" ].Text = "&Done"
            );

            _wizard.LockNext();
            _wizard.LockPrev(); // No going back now!

            DetermineFate();

            //BusyForm.Dismiss();
            Run( MainLoop );
        }

#if xDEBUG
        private void DEBUG_FakeData()
        {
            MainForm.State.Device = new AnalyzerDeviceTEST_SAE_Bad( "DEBUG" );
            /* DEBUG */
            MainForm.State.TechName = "Debug Jones";
            MainForm.State.PrelimInspectFilter = true;
            MainForm.State.PrelimPoweredUp = true;
            MainForm.State.FlowMeterReading = 53.2f;
            MainForm.State.Notes = "These are my debug notes, brother.";
            MainForm.State.SerNo = "0xD3ADB33F";

            MainForm.State.Device.CmdDoCalibration();

            foreach ( var foo in MainForm.State.Device.LastResults ) {
                Debug.WriteLine( foo );
            }

            MainForm.State.ResultsGasAnal.Raw = MainForm.State.Device.CmdDoAnalysis();
            Debug.WriteLine( "Gas Anal = " + MainForm.State.ResultsGasAnal );

            MainForm.State.ResultsAirAnal.Raw = MainForm.State.Device.CmdDoAnalysis();
            Debug.WriteLine( "Air Anal = " + MainForm.State.ResultsAirAnal );

            Debug.WriteLine( MainForm.State.Device );

            // BAD TEST
            //MainForm.State.ResultsGasAnal.Raw = "003.0 088.5 001.5 001.0 002.0 00000 02527";
            //MainForm.State.ResultsAirAnal.Raw = "003.0 010.0 001.5 001.0 040.0 00000 02528";

            // GOOD TEST
            //MainForm.State.ResultsGasAnal.Raw = "003.0 100.0 001.5 001.0 000.0 00000 02527";
            //MainForm.State.ResultsAirAnal.Raw = "003.0 000.0 001.5 001.0 100.0 00005 02527";

            //MainForm.State.PreviousResults.Clear();
            //MainForm.State.PreviousResults.Add( "003.0 094.5 001.5 001.0 002.0 00000 02520" );
            //MainForm.State.PreviousResults.Add( "003.1 094.5 001.5 001.0 002.1 00000 02521" );
            //MainForm.State.PreviousResults.Add( "003.2 094.5 001.5 001.0 002.2 00000 02522" );
            //MainForm.State.PreviousResults.Add( "003.3 094.5 001.5 001.0 002.3 00000 02523" );
            //MainForm.State.PreviousResults.Add( "003.4 094.5 001.5 001.0 002.4 00000 02524" );
        }
#endif
        //public override void OnExit()
        //{
        //    _parent_form.Controls[ "btnNext" ].SynchronizedInvoke( () =>
        //            _parent_form.Controls[ "btnNext" ].Text = "&Next"
        //    );
        //}


        private void DetermineFate()
        {
            DisableValidation = false;
            // PASS
            Label body = (Label)_parent_form
                .Controls[ "tabControl1" ]
                .Controls[ "tabPage10" ]
                .Controls[ "panel1" ]
                .Controls[ "lblResultsBody" ];

            Label header = (Label)_parent_form
                .Controls[ "tabControl1" ]
                .Controls[ "tabPage10" ]
                .Controls[ "panel1" ]
                .Controls[ "lblResultsHead" ];

            CheckBox cb_latest = (CheckBox)_parent_form
                .Controls[ "tabControl1" ]
                .Controls[ "tabPage10" ]
                .Controls[ "panel1" ]
                .Controls[ "cbLatestFirmware" ];

            if ( MainForm.State.IdentifierDetectionFailed ) {
                header.Text = "There was a problem searching for an identifier...";
                header.ForeColor = Color.Goldenrod;
                body.Text = MainForm.State.IdentifierDetectionFailureMessage;
                cb_latest.Visible = false;
                DisableValidation = true;
                _parent_form.Controls[ "btnNext" ].SynchronizedInvoke( () => {
                    //_parent_form.Controls[ "btnNext" ].Text = "&Done";
                    _parent_form.Controls[ "btnNext" ].Enabled = true;
                } );
            }
            else if ( !MainForm.State.HasErrors() ) {
                header.Text = "Diagnostics have completed, and the application found NO errors.";
                header.ForeColor = Color.ForestGreen;

                body.Text = "Press [DONE] to exit the application and display the report.";
                _cb_latest_firmware.Checked = true;
                _cb_latest_firmware.Visible = false;
            }
            else {
                header.Text = "Diagnostics have completed, and the application FOUND ERRORS.";
                header.ForeColor = Color.DarkRed;

                body.Text = "Troubleshooting protocol specifies that you update the device's software and repeat the test.\n\n" +
                            "If you have already done so, please check the box below.\n\n" +
                            "Press [DONE] to exit the application and display the report.";
                _cb_latest_firmware.Checked = false;
                _cb_latest_firmware.Visible = true;

            }
        }
    }
}
