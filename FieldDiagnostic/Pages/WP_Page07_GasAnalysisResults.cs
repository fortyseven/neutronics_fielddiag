﻿using System.Windows.Forms;
using FieldDiagnostic.Device;

namespace FieldDiagnostic.Pages
{
    /*
     * Page 7 - Gas Analysis RESULTS
     */

    class WP_Page07_GasAnalysisResults : IWizardPage
    {
        public WP_Page07_GasAnalysisResults( WizardController wizard )
            : base( wizard )
        {
        }

        override
        public void OnPageEnter()
        {
            //MainForm.State.ResultsGasAnal.Raw = "000.0 000.0 000.0 000.0 100.0 00005 00023";

            DataGridView grid = _parent_form.Controls[ "tabControl1" ].Controls[ "tabPage7" ].Controls[ "grid7_GasAnal" ] as DataGridView;

            if (grid == null) {
                MessageBox.Show("Error drawing grid. This should never happen. Press NEXT to proceed.");
                return;
            }

            if ( MainForm.State.Device.DeviceType == AnalyzerDevice.Type.VDA ) {
                grid.SynchronizedInvoke( () => {
                    grid.RowCount = 2;
                    grid[ 0, 0 ].Value = "Result Code:";
                    grid[ 1, 0 ].Value = MainForm.State.ResultsGasAnal.ResultCode;
                    grid[ 0, 1 ].Value = "Reading Number:";
                    grid[ 1, 1 ].Value = MainForm.State.ResultsGasAnal.ReadingNumber;
                } );
            }
            else {
                grid.SynchronizedInvoke( () => {
                    grid.RowCount = 7;
                    grid[ 0, 0 ].Value = "R134a:";
                    grid[ 1, 0 ].Value = MainForm.State.ResultsGasAnal.R134a;
                    grid[ 0, 1 ].Value = "R1234yf:";
                    grid[ 1, 1 ].Value = MainForm.State.ResultsGasAnal.R1234yf;
                    grid[ 0, 2 ].Value = "R22:";
                    grid[ 1, 2 ].Value = MainForm.State.ResultsGasAnal.R22;
                    grid[ 0, 3 ].Value = "HC:";
                    grid[ 1, 3 ].Value = MainForm.State.ResultsGasAnal.HC;
                    grid[ 0, 4 ].Value = "Air:";
                    grid[ 1, 4 ].Value = MainForm.State.ResultsGasAnal.Air;
                    grid[ 0, 5 ].Value = "Result Code:";
                    grid[ 1, 5 ].Value = MainForm.State.ResultsGasAnal.ResultCode;
                    grid[ 0, 6 ].Value = "Reading Number:";
                    grid[ 1, 6 ].Value = MainForm.State.ResultsGasAnal.ReadingNumber;
                } );
            }


            _wizard.UnlockPrev();
            _wizard.UnlockNext();
        }
    }
}
