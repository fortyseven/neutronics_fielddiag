﻿using System;
using System.Windows.Forms;
using FieldDiagnostic.Device;
using FieldDiagnostic.Pages.UI;

namespace FieldDiagnostic.Pages
{
    /*
     * Page 6 - Gas Analysis
     */

    class WP_Page06_GasAnalysis : IWizardPage
    {
        private const int BUSY_WAIT_SECS = 62;

        public WP_Page06_GasAnalysis( WizardController wizard )
            : base( wizard )
        {
        }

        private void InitThread()
        {
            _wizard.LockNext();

            // Check if recalibration is necessary; do that first if so

            if ( MainForm.State.Device.CmdSystemCheck() == AnalyzerDevice.Response.Q_REQCALIBRATION ) {
                MessageBox.Show( "Analyzer calibration data has expired and must be recalibrated. Please configure the analyzer for air analysis before proceeding.", "Recalibration Required", MessageBoxButtons.OK, MessageBoxIcon.Stop );
                BusyForm.Show( "Performing recalibration...please wait.", WP_Page04_AirCalibration.BUSY_DELAY_SECS );
                MainForm.State.Device.CmdDoCalibration();
                BusyForm.Dismiss();
                MessageBox.Show( "Analyzer has been recalibrated. Return the device to it's original state to resume gas analysis.", "Recalibration Complete", MessageBoxButtons.OK, MessageBoxIcon.Information );
            }

            //

            BusyForm.Show( "Performing gas analysis...please wait.", BUSY_WAIT_SECS );
            try {
                var result = MainForm.State.Device.CmdDoAnalysis();
                //var result = "000.0 000.0 000.0 000.0 100.0 00005 00023";

                if ( result == null )
                    throw new Exception( "Device returned unknown response." );

                MainForm.State.ResultsGasAnal.Raw = result;

                BusyForm.Dismiss();
                _wizard.NextForce();
            }
            catch ( Exception e ) {
                BusyForm.Dismiss();
                MessageBox.Show( "There was a problem performing gas analysis.\n" + e.Message, "Analysis Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error );
                _wizard.UnlockNext();
            }
        }

        /*
                private void OldInitThread()
                {
                    BusyForm.Show( "Performing gas analysis...please wait.", BUSY_WAIT_SECS );
                    try {
                        var result = MainForm.State.Device.CmdDoAnalysis();
                        if ( result != null ) {
                            MainForm.State.Log.Write( "Gas analysis completed: " + result );
                            try {
                                MainForm.State.ResultsGasAnal.Raw = result;
                                // BEST OF ALL POSSIBLE RESULTS 
                                //_wizard.NextForce();
                            }
                            catch ( Exception e ) {
                                BusyForm.Dismiss();
                                MainForm.State.Log.Write( "Problem parsing gas analysis repsponse: " + e.Message );
                                MessageBox.Show( "There was a problem reading gas analysis results.\n" + e.Message, "Failure",
                                                                        MessageBoxButtons.OK, MessageBoxIcon.Asterisk );
                                return;
                            }
                        }
                        else {
                            BusyForm.Dismiss();
                            MessageBox.Show( "There was a problem performing gas analysis. Check the log for details.", "Failure",
                                                                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk );
                            return;
                        }
                    }
                    catch ( Exception e ) {
                        BusyForm.Dismiss();
                        MainForm.State.Log.Write( "Device error: " + e.Message );
                        MessageBox.Show( e.Message, "Device Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
                        return;
                    }
                    finally {
                        _wizard.UnlockNext();
                        _wizard.UnlockPrev();
                    }
                    _wizard.NextForce();
                }
        */

        public override void OnPageEnter()
        {
            _wizard.UnlockPrev();
            _wizard.UnlockNext();
        }

        public override bool OnPageExit()
        {
            _wizard.LockPrev();
            _wizard.LockNext();
            Run( InitThread );
            return false;
        }
    }
}
