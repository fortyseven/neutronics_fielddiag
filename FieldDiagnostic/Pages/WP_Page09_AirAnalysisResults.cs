﻿using FieldDiagnostic.Device;
using System.Windows.Forms;

namespace FieldDiagnostic.Pages
{
    /*
     * Page 9 - Air Analysis Results
     */

    internal class WP_Page09_AirAnalysisResults : IWizardPage
    {
        public WP_Page09_AirAnalysisResults( WizardController wizard )
            : base( wizard )
        {
        }

        public
        override void OnPageEnter()
        {
            //MainForm.State.ResultsAirAnal.Raw = "000.0 000.0 000.0 000.0 100.0 00005 00023";

            DataGridView grid =
                _parent_form.Controls[ "tabControl1" ].Controls[ "tabPage9" ].Controls[ "grid9_AirAnal" ] as DataGridView;

            if ( grid == null ) {
                MessageBox.Show( "Error drawing grid. This should never happen. Press NEXT to proceed." );
                return;
            }

            if ( MainForm.State.Device.DeviceType == AnalyzerDevice.Type.VDA ) {
                grid.SynchronizedInvoke( () => {
                    grid.RowCount = 2;
                    grid[ 0, 0 ].Value = "Result Code:";
                    grid[ 1, 0 ].Value = MainForm.State.ResultsAirAnal.ResultCode;
                    grid[ 0, 1 ].Value = "Reading Number:";
                    grid[ 1, 1 ].Value = MainForm.State.ResultsAirAnal.ReadingNumber;
                } );
            }
            else {
                grid.SynchronizedInvoke( () => {

                    grid.RowCount = 7;

                    grid[ 0, 0 ].Value = "R134a:";
                    grid[ 1, 0 ].Value = MainForm.State.ResultsAirAnal.R134a;
                    grid[ 0, 1 ].Value = "R1234yf:";
                    grid[ 1, 1 ].Value = MainForm.State.ResultsAirAnal.R1234yf;
                    grid[ 0, 2 ].Value = "R22:";
                    grid[ 1, 2 ].Value = MainForm.State.ResultsAirAnal.R22;
                    grid[ 0, 3 ].Value = "HC:";
                    grid[ 1, 3 ].Value = MainForm.State.ResultsAirAnal.HC;
                    grid[ 0, 4 ].Value = "Air:";
                    grid[ 1, 4 ].Value = MainForm.State.ResultsAirAnal.Air;
                    grid[ 0, 5 ].Value = "Result Code:";
                    grid[ 1, 5 ].Value = MainForm.State.ResultsAirAnal.ResultCode;
                    grid[ 0, 6 ].Value = "Reading Number:";
                    grid[ 1, 6 ].Value = MainForm.State.ResultsAirAnal.ReadingNumber;
                } );
            }
            _wizard.UnlockPrev();
            _wizard.UnlockNext();
        }
    }
}
