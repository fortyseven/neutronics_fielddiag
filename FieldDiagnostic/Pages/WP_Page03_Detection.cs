﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using FieldDiagnostic.Device;
using FieldDiagnostic.Diags;
using FieldDiagnostic.Pages.UI;

namespace FieldDiagnostic.Pages
{
    /*
     * Page 3 - Initializing Analyzer
     */
    class WP_Page03_Detection : IWizardPage
    {
        private DeviceDetection       _init_diag;
        private bool SkipToErrorPage { get; set; }

        /*************************************************************************/
        public WP_Page03_Detection( WizardController wizard )
            : base( wizard )
        {
        }

        /*************************************************************************/
        private void InitThread()
        {
            Debug.WriteLine( "InitThead Started" );
            BusyForm.Show( "Searching for devices...", -1, () => {
                _init_diag.Cancel();
            } );

            _init_diag = new DeviceDetection();
            _init_diag.SetOnFinishCallback( OnInitFinished );
            _init_diag.BeginDetectionThread();
        }

        /*************************************************************************/
        private void OnInitFinished( AnalyzerDevice device )
        {
            Debug.WriteLine( "OnInitFinished" );
            if ( _init_diag.WasCanceled ) {
                Debug.WriteLine( "    Was canceled." );
                BusyForm.Dismiss();
                MessageBox.Show( "Device detection canceled.", "Abort", MessageBoxButtons.OK, MessageBoxIcon.Stop );
                _wizard.UnlockNext();
            }
            else if ( device != null ) {
                try {
                    Debug.WriteLine( "    Device found...." );
                    device.GatherDeviceDetails();
                    BusyForm.Dismiss();
                    MainForm.State.Device = device;
                    MessageBox.Show( "Found " + device.DeviceName + " device on " + device.PortName +
                                    "\nRev: " + device.SoftwareRevision +
                                    "\nSerNo: " + device.SerialNumber +
                                    "\n\nPress OK to continue."
                        , "Device Found", MessageBoxButtons.OK, MessageBoxIcon.Information );

                    _wizard.NextForce();
                }
                catch ( Exception e ) {
                    Debug.WriteLine( "Exception: " + e.Message + " - " + Thread.CurrentThread.Name );
                    FatalFinish( "A device was discovered on '" + device.PortName + "', but there was an error querying the device details: \n\n" + e.Message );
                    return;
                }
            }
            else {
                FatalFinish( _init_diag.GetErrors() );
                return;
            }
            BusyForm.Dismiss();
            Stop();
        }

        /*************************************************************************/
        private void FatalFinish( string errors )
        {
            Debug.WriteLine( "FATAL: " + errors );

            BusyForm.Dismiss();

            //   StringBuilder msg = new StringBuilder();
            string msg = "";

            if ( errors.Length > 0 ) {
                msg += "Results:\n\n" + errors;
            }

            MainForm.State.SetDetectionFailed( msg );

            Stop();

            SkipToErrorPage = true;

            GetParentForm().Invoke( new Action( () => {
                _wizard.SetPage( 10 - 1, true );
            } ) );
        }

        /*************************************************************************/
        public override void OnPageEnter()
        {
            // Close the port, if we already have one -- safety
            if ( MainForm.State.Device != null ) {
                MainForm.State.Device.Done();
            }
            MainForm.State.Device = null;
            SkipToErrorPage = false;
            _wizard.UnlockPrev();
        }

        /*************************************************************************/
        public override bool OnPageExit()
        {
            if ( SkipToErrorPage ) {
                _wizard.SetPage( 10 - 1, true );
                return false;
            }
            _wizard.LockPrev();
            _wizard.LockNext();
            InitThread();
            return false;
        }
    }
}
