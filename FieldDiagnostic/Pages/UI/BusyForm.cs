﻿using System;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using Application = System.Windows.Forms.Application;
using MessageBox = System.Windows.MessageBox;

namespace FieldDiagnostic.Pages.UI
{
    public partial class BusyForm : Form
    {
        private static bool _running;
        private static string _set_text_to;
        private static Thread _t;
        private static BusyForm _busyform;
        private static int _estimated_time_in_ms;
        private static bool _was_suspended;
        private static OnCancelCallback _cancel_callback;
        private static string _set_caption_to;

        public delegate void OnCancelCallback();

        /********************************************************/
        public BusyForm()
        {
            InitializeComponent();
        }

        /********************************************************/
        public void SetText( string text )
        {
            lblActivityDescription.Text = text;
        }

        /********************************************************/
        private static void BusyThread()
        {
            int start_ticks = Environment.TickCount;

            Thread.CurrentThread.Name = "BusyForm.BusyThread";

            _busyform = new BusyForm();
            _busyform.SetText( _set_text_to );

            if ( _estimated_time_in_ms >= 0 ) {
                _busyform.progressBar1.Minimum = 0;
                _busyform.progressBar1.Maximum = _estimated_time_in_ms;
                _busyform.progressBar1.Style = ProgressBarStyle.Blocks;
            }
            else {
                _busyform.progressBar1.Style = ProgressBarStyle.Marquee;
                _busyform.lblEstTime.Text = "";
                _busyform.lblEstTimeValue.Text = @"Operations are in progress, please wait...";
                _busyform.progressBar1.MarqueeAnimationSpeed = 50;
            }
            _busyform.TopMost = true;
            _busyform.TopLevel = true;
            _busyform.Show();

            if ( _set_caption_to != null ) {
                SetCaption( _set_caption_to );
            }

            StringBuilder est_time_text = new StringBuilder( 60 );

            while ( _running ) {
                if ( _was_suspended ) {
                    Thread.Sleep( 100 );
                    Application.DoEvents();
                }
                else {
                    try {
                        if ( _estimated_time_in_ms >= 0 ) {
                            long cur_ticks = ( start_ticks + _estimated_time_in_ms ) - Environment.TickCount;

                            if ( cur_ticks < 0 ) {
                                _busyform.lblEstTimeValue.Text =
                                    "Operation is taking longer than expected. Still working.";
                            }
                            else {
                                TimeSpan time_span = TimeSpan.FromTicks( cur_ticks * TimeSpan.TicksPerMillisecond );

                                est_time_text.Length = 0; // We'd use Clear() in 4.0

                                if ( time_span.Minutes > 0 )
                                    est_time_text.Append( time_span.Minutes + "m " );

                                est_time_text.Append( time_span.Seconds + "s " );

                                _busyform.lblEstTimeValue.Text = est_time_text.ToString();

                                var ticks = cur_ticks;
                                _busyform.progressBar1.Invoke(
                                    (Action)
                                        delegate {
                                            _busyform.progressBar1.Value = _estimated_time_in_ms - (int)ticks;
                                        } );
                            }
                        }
                        // This is for ongoing events
                        else {
                            //_busyform.progressBar1.PerformStep();
                        }
                        Thread.Sleep( 0 );
                        Application.DoEvents();
                    }
                    catch ( ThreadInterruptedException ) {
                        _was_suspended = true;
                        return;
                    }
                }
            }

            _busyform.Close();
        }

        /********************************************************/
        public static void Dismiss()
        {
            _running = false;
            _was_suspended = true;
        }

        /********************************************************/
        public static void SetCaption( string title )
        {
            if ( _busyform == null ) {
                _set_caption_to = title;

            }
            else {
                _busyform.SynchronizedInvoke( delegate { _busyform.Text = title; } );
            }
        }
        /********************************************************/
        public static void Show( string msg, int time_in_secs = -1, OnCancelCallback cancel_callback = null )
        {
            _running = true;
            _was_suspended = false;

            if ( time_in_secs < 0 ) {
                // This is a continuously running process with no defined estimated time
                _estimated_time_in_ms = -1;
            }
            else {
                _estimated_time_in_ms = time_in_secs * 1000;
            }
            _cancel_callback = cancel_callback;
            _set_text_to = msg;
            _set_caption_to = null;

            _t = new Thread( BusyThread );
            _t.Start();
        }

        /********************************************************/
        private void BusyForm_FormClosing( object sender, FormClosingEventArgs event_args )
        {
            if ( !_running )
                return;

            if ( _cancel_callback == null ) {
                event_args.Cancel = true;
                return;
            }

            if ( _was_suspended ) // We're closing from a 'Dismiss'
                return;

            _was_suspended = true;
            _busyform.TopMost = false;

            new Thread( new ThreadStart( delegate {
                if ( MessageBox.Show( "Are you sure you want to abort this operation?",
                                      "Abort?",
                                      MessageBoxButton.YesNo,
                                      MessageBoxImage.Question ) == MessageBoxResult.No ) {
                    _busyform.SynchronizedInvoke( () => _busyform.TopMost = true );
                    _busyform.SynchronizedInvoke( () => _busyform.Focus() );
                    _was_suspended = false;
                }
                else {
                    Dismiss();
                    if ( _cancel_callback != null )
                        _cancel_callback();
                }
            } ) ).Start();

            event_args.Cancel = true;
        }
    }
}
