﻿using System;
using System.Windows.Forms;
using FieldDiagnostic.Pages.UI;

namespace FieldDiagnostic.Pages
{
    /*
     * Page 4 - Prepare for Air Calibration
     */

    class WP_Page04_AirCalibration : IWizardPage
    {
        public const int BUSY_DELAY_SECS = 32;

        public WP_Page04_AirCalibration( WizardController wizard )
            : base( wizard )
        {
        }

        private void InitThread()
        {
            BusyForm.Show( "Performing calibration...please wait.", BUSY_DELAY_SECS );
            try {
                MainForm.State.Device.CmdDoCalibration();
                //throw new Exception("baloney");
                BusyForm.Dismiss();
                _wizard.NextForce();
            }
            catch ( Exception e ) {
                BusyForm.Dismiss();
                MessageBox.Show( "There was a problem performing air calibration.\n" + e.Message, "Calibration Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
                _wizard.UnlockPrev();
                _wizard.UnlockNext();
            }
        }
        public override void OnPageEnter()
        {
            _wizard.UnlockPrev();
            _wizard.UnlockNext();
        }

        public override bool OnPageExit()
        {
            _wizard.LockPrev();
            _wizard.LockNext();
            Run( InitThread );
            return false;
        }
    }
}
