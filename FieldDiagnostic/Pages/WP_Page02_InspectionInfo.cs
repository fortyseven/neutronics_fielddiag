﻿using System.Threading;
using System.Windows.Forms;

namespace FieldDiagnostic.Pages
{
    /*
     * Page 2 - Visual Inspection
     */
    class WP_Page02_InspectionInfo : IWizardPage
    {
        private Control.ControlCollection _page2_controls;

        private CheckBox _cb2_powered_up, 
                         _cb2_inspect_filter, 
                         _cb2_inspect_line,
                         _cb2_replaced_brass;

        private TextBox _tb2_technician, 
                        _tb2_serial_no, 
                        _tb2_notes;

        public WP_Page02_InspectionInfo( WizardController wizard )
            : base( wizard )
        {
        }

        private void MainLoop()
        {
            while ( Running ) {
                int fields_filled = 0;

                if ( _tb2_technician.Text.Trim().Length > 0 )
                    fields_filled++;
                if ( _tb2_serial_no.Text.Trim().Length > 0 )
                    fields_filled++;

                if ( fields_filled == 2 )
                    _wizard.UnlockNext();
                else
                    _wizard.LockNext();

                Thread.Sleep( 100 );
            }
        }

        public override void OnPageEnter()
        {
            // Would love to do this earlier, but...
            _page2_controls = _parent_form.Controls[ "tabControl1" ].Controls[ "tabPage2" ].Controls[ "pan2" ].Controls;

            _cb2_powered_up = _page2_controls[ "cb2_PoweredUp" ] as CheckBox;
            _cb2_inspect_filter = _page2_controls[ "cb2_InspectFilter" ] as CheckBox;
            _cb2_inspect_line = _page2_controls[ "cb2_InspectLine" ] as CheckBox;
            _cb2_replaced_brass = _page2_controls[ "cb2_ReplacedBrass" ] as CheckBox;

            _tb2_technician = _page2_controls[ "tb2_Technician" ] as TextBox;
            _tb2_serial_no = _page2_controls[ "tb2_SerialNo" ] as TextBox;
            _tb2_notes = _page2_controls[ "tb2_Notes" ] as TextBox;

            _wizard.UnlockPrev();
            _wizard.LockNext();

            Run( MainLoop );
        }

        public override bool OnPageExit()
        {
            int _num_checked = 0;
            
            if ( _cb2_powered_up.Checked )
                _num_checked++;
            if ( _cb2_inspect_filter.Checked )
                _num_checked++;
            if ( _cb2_inspect_line.Checked )
                _num_checked++;
            if ( _cb2_replaced_brass.Checked )
                _num_checked++;

            if ( _num_checked < 4 ) {
                if ( MessageBox.Show(
                    "Physical issues checklist is incomplete. Proceed anyway? ",
                    "Are you sure?",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation ) == DialogResult.No )
                    return false;
            }

            MainForm.State.TechName = _tb2_technician.Text.Trim();
            MainForm.State.SerNo = _tb2_serial_no.Text.Trim();
            MainForm.State.Notes = _tb2_notes.Text.Trim();

            MainForm.State.PrelimPoweredUp = _cb2_powered_up.Checked;
            MainForm.State.PrelimInspectFilter = _cb2_inspect_filter.Checked;
            MainForm.State.PrelimInspectLine = _cb2_inspect_line.Checked;
            MainForm.State.PrelimReplacedBrass = _cb2_replaced_brass.Checked;

            // log it
            //MainForm.State.Log.Write( "-------------------------------------------" );
            //MainForm.State.Log.Write( "Technician Name: " + _tb2_technician.Text.Trim() );
            //MainForm.State.Log.Write( "Serial Number  : " + _tb2_serial_no.Text.Trim() );
            //MainForm.State.Log.Write( "Notes          : " + _tb2_notes.Text.Trim() );
            //MainForm.State.Log.Write( "-------------------------------------------" );
            //MainForm.State.Log.Write( "Powered up?            : " + ( _cb2_powered_up.Checked ? "Y" : "N" ) );
            //MainForm.State.Log.Write( "Inspected filter?      : " + ( _cb2_inspect_filter.Checked ? "Y" : "N" ) );
            //MainForm.State.Log.Write( "Inspected sample line? : " + ( _cb2_inspect_line.Checked ? "Y" : "N" ) );
            //MainForm.State.Log.Write( "Replaced filters?      : " + ( _cb2_replaced_brass.Checked ? "Y" : "N" ) );
            //MainForm.State.Log.Write( "" );

            return base.OnPageExit();
        }
    }
}
