﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace FieldDiagnostic.Pages
{
    /*
     * Page 5 - Gas Flow Verification
     */

    class WP_Page05_FlowMeterReading : IWizardPage
    {
        private Control.ControlCollection _page5_controls;
        private float? _flowmeter_value;
        TextBox _textbox_flow;
        // Within your class or scoped in a more appropriate location:
        [DllImport( "user32.dll" )]
        private static extern IntPtr SendMessage( IntPtr hWnd, int Msg, int wParam, [MarshalAs( UnmanagedType.LPWStr )] string lParam );

        public WP_Page05_FlowMeterReading( WizardController wizard )
            : base( wizard )
        {
        }

        private void MainLoop()
        {
            _textbox_flow.KeyDown += ( obj, key ) => {
                if ( key.KeyCode != Keys.Return )
                    return;

                if ( _flowmeter_value != null ) {
                    _wizard.NextForce();
                }
            };

            // Ensure the cursor is on the input box
            _textbox_flow.SynchronizedInvoke( () => _textbox_flow.Focus() );

            while ( Running ) {
                string val = _textbox_flow.Text.Trim();

                // Try and convert it into a float
                _flowmeter_value = ValidateEntry( val );

                if ( _flowmeter_value == null )
                    _wizard.LockNext();
                else
                    _wizard.UnlockNext();

                Thread.Sleep( 100 );
            }
        }

        private float? ValidateEntry( string val )
        {
            if ( val.Trim().Length == 0 )
                return null;

            try {
                return Convert.ToSingle( val );
            }
            catch {
                return null;
            }
        }


        public override void OnPageEnter()
        {
            _page5_controls = _parent_form.Controls[ "tabControl1" ].Controls[ "tabPage5" ].Controls[ "pan5" ].Controls;


            _textbox_flow = (TextBox)( _page5_controls[ "tb5_FlowMeter" ] );

            _textbox_flow.SynchronizedInvoke( () => SendMessage( _textbox_flow.Handle, 0x1501, 1, "Enter a valid numeric value to proceed." ) );

            _wizard.UnlockPrev();
            _wizard.LockNext();
            //_wizard.LockPrev();
            Run( MainLoop );
        }

        public override bool OnPageExit()
        {
            if ( _flowmeter_value == null )
                return false;

            Stop();
            MainForm.State.FlowMeterReading = _flowmeter_value.Value;
            return base.OnPageExit();
        }
    }
}
