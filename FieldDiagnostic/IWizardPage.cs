﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace FieldDiagnostic
{
    abstract class IWizardPage
    {
        protected readonly WizardController _wizard;
        private Thread _pagethread;
        protected Form _parent_form;

        protected bool Running { get; private set; }

        protected IWizardPage( WizardController wizard )
        {
            _wizard = wizard;
        }

        protected void Run( Action target )
        {
            Running = true;
            _pagethread = new Thread( new ThreadStart( target ) ) {
                Name = "Wizard Page Thread (" + ToString() + ")"
            };
            _pagethread.Start();
        }

        public void Stop()
        {
            Running = false;
        }

        public void OnPageChange()
        {
            Stop();
        }

        virtual public void OnPageEnter()
        {
        }

        virtual public bool OnPageExit()
        {
            Stop();
            return true;
        }

        internal void SetParentForm( Form parent_form )
        {
            _parent_form = parent_form;
        }

        public Form GetParentForm()
        {
            return _parent_form;
        }

        virtual public void OnExit()
        {
        }
    }
}
