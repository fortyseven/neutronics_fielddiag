﻿namespace FieldDiagnostic
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.lbl1_Instruct = new System.Windows.Forms.Label();
            this.imgBrandLogo = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lbl1_Header = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pan2 = new System.Windows.Forms.Panel();
            this.tb2_Notes = new System.Windows.Forms.TextBox();
            this.tb2_SerialNo = new System.Windows.Forms.TextBox();
            this.tb2_Technician = new System.Windows.Forms.TextBox();
            this.lbl2_TechnicianNotes = new System.Windows.Forms.Label();
            this.lbl2_DeviceSerNo = new System.Windows.Forms.Label();
            this.lbl2_Name = new System.Windows.Forms.Label();
            this.cb2_ReplacedBrass = new System.Windows.Forms.CheckBox();
            this.cb2_InspectLine = new System.Windows.Forms.CheckBox();
            this.cb2_InspectFilter = new System.Windows.Forms.CheckBox();
            this.cb2_PoweredUp = new System.Windows.Forms.CheckBox();
            this.lbl2_Instruct = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lbl3_Instruct = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.lbl4_Header = new System.Windows.Forms.Label();
            this.lbl4_Instruct = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.lbl5_Header = new System.Windows.Forms.Label();
            this.lbl5_Instruct = new System.Windows.Forms.Label();
            this.pan5 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tb5_FlowMeter = new System.Windows.Forms.TextBox();
            this.lbl5_FlowMeter = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.lbl6_Header = new System.Windows.Forms.Label();
            this.lbl6_Instruct = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.lbl7_Header = new System.Windows.Forms.Label();
            this.grid7_GasAnal = new System.Windows.Forms.DataGridView();
            this.ColProperty7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColValue7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.lbl8_Header = new System.Windows.Forms.Label();
            this.lbl8_Instruct = new System.Windows.Forms.Label();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.lbl9_Header = new System.Windows.Forms.Label();
            this.grid9_AirAnal = new System.Windows.Forms.DataGridView();
            this.ColProperty9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColValue9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbLatestFirmware = new System.Windows.Forms.CheckBox();
            this.lblResultsHead = new System.Windows.Forms.Label();
            this.lblResultsBody = new System.Windows.Forms.Label();
            this.progBarPage = new System.Windows.Forms.ProgressBar();
            this.lblPage = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgBrandLogo)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.pan2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.pan5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid7_GasAnal)).BeginInit();
            this.tabPage8.SuspendLayout();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid9_AirAnal)).BeginInit();
            this.tabPage10.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPrev
            // 
            this.btnPrev.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnPrev.Location = new System.Drawing.Point(12, 406);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(75, 23);
            this.btnPrev.TabIndex = 0;
            this.btnPrev.Text = "< &Prev";
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnNext
            // 
            this.btnNext.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNext.Location = new System.Drawing.Point(537, 406);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 1;
            this.btnNext.Text = "&Next >";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lbl1_Instruct
            // 
            this.lbl1_Instruct.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbl1_Instruct.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1_Instruct.Location = new System.Drawing.Point(3, 60);
            this.lbl1_Instruct.Name = "lbl1_Instruct";
            this.lbl1_Instruct.Size = new System.Drawing.Size(428, 328);
            this.lbl1_Instruct.TabIndex = 2;
            this.lbl1_Instruct.Text = "This program will walk you through the steps required to test the R1234yf Identif" +
    "ier, to ensure it is functioning correctly and to generate a test report.";
            // 
            // imgBrandLogo
            // 
            this.imgBrandLogo.ErrorImage = null;
            this.imgBrandLogo.ImageLocation = "logo.jpg";
            this.imgBrandLogo.Location = new System.Drawing.Point(4, 5);
            this.imgBrandLogo.Name = "imgBrandLogo";
            this.imgBrandLogo.Size = new System.Drawing.Size(162, 162);
            this.imgBrandLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBrandLogo.TabIndex = 3;
            this.imgBrandLogo.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.ItemSize = new System.Drawing.Size(0, 1);
            this.tabControl1.Location = new System.Drawing.Point(170, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(442, 400);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tabPage1.Controls.Add(this.lbl1_Header);
            this.tabPage1.Controls.Add(this.lbl1_Instruct);
            this.tabPage1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.tabPage1.Location = new System.Drawing.Point(4, 5);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(434, 391);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            // 
            // lbl1_Header
            // 
            this.lbl1_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl1_Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1_Header.Location = new System.Drawing.Point(3, 3);
            this.lbl1_Header.Name = "lbl1_Header";
            this.lbl1_Header.Size = new System.Drawing.Size(428, 42);
            this.lbl1_Header.TabIndex = 3;
            this.lbl1_Header.Text = "Welcome to the Refrigerant Analyzer Diagnostic tool for SAE and VDA Identifiers.";
            this.lbl1_Header.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pan2);
            this.tabPage2.Controls.Add(this.lbl2_Instruct);
            this.tabPage2.Location = new System.Drawing.Point(4, 5);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(434, 391);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pan2
            // 
            this.pan2.Controls.Add(this.tb2_Notes);
            this.pan2.Controls.Add(this.tb2_SerialNo);
            this.pan2.Controls.Add(this.tb2_Technician);
            this.pan2.Controls.Add(this.lbl2_TechnicianNotes);
            this.pan2.Controls.Add(this.lbl2_DeviceSerNo);
            this.pan2.Controls.Add(this.lbl2_Name);
            this.pan2.Controls.Add(this.cb2_ReplacedBrass);
            this.pan2.Controls.Add(this.cb2_InspectLine);
            this.pan2.Controls.Add(this.cb2_InspectFilter);
            this.pan2.Controls.Add(this.cb2_PoweredUp);
            this.pan2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pan2.Font = new System.Drawing.Font("Tahoma", 9F);
            this.pan2.Location = new System.Drawing.Point(3, 135);
            this.pan2.Name = "pan2";
            this.pan2.Size = new System.Drawing.Size(428, 253);
            this.pan2.TabIndex = 1;
            // 
            // tb2_Notes
            // 
            this.tb2_Notes.Location = new System.Drawing.Point(4, 208);
            this.tb2_Notes.Multiline = true;
            this.tb2_Notes.Name = "tb2_Notes";
            this.tb2_Notes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb2_Notes.Size = new System.Drawing.Size(424, 43);
            this.tb2_Notes.TabIndex = 9;
            // 
            // tb2_SerialNo
            // 
            this.tb2_SerialNo.Location = new System.Drawing.Point(4, 164);
            this.tb2_SerialNo.Name = "tb2_SerialNo";
            this.tb2_SerialNo.Size = new System.Drawing.Size(424, 22);
            this.tb2_SerialNo.TabIndex = 8;
            // 
            // tb2_Technician
            // 
            this.tb2_Technician.Location = new System.Drawing.Point(4, 122);
            this.tb2_Technician.Name = "tb2_Technician";
            this.tb2_Technician.Size = new System.Drawing.Size(424, 22);
            this.tb2_Technician.TabIndex = 7;
            // 
            // lbl2_TechnicianNotes
            // 
            this.lbl2_TechnicianNotes.AutoSize = true;
            this.lbl2_TechnicianNotes.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl2_TechnicianNotes.Location = new System.Drawing.Point(1, 190);
            this.lbl2_TechnicianNotes.Name = "lbl2_TechnicianNotes";
            this.lbl2_TechnicianNotes.Size = new System.Drawing.Size(179, 14);
            this.lbl2_TechnicianNotes.TabIndex = 6;
            this.lbl2_TechnicianNotes.Text = "Technician Notes: (Optional)";
            // 
            // lbl2_DeviceSerNo
            // 
            this.lbl2_DeviceSerNo.AutoSize = true;
            this.lbl2_DeviceSerNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl2_DeviceSerNo.Location = new System.Drawing.Point(1, 146);
            this.lbl2_DeviceSerNo.Name = "lbl2_DeviceSerNo";
            this.lbl2_DeviceSerNo.Size = new System.Drawing.Size(138, 14);
            this.lbl2_DeviceSerNo.TabIndex = 5;
            this.lbl2_DeviceSerNo.Text = "Device Serial Number:";
            // 
            // lbl2_Name
            // 
            this.lbl2_Name.AutoSize = true;
            this.lbl2_Name.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl2_Name.Location = new System.Drawing.Point(1, 105);
            this.lbl2_Name.Name = "lbl2_Name";
            this.lbl2_Name.Size = new System.Drawing.Size(183, 14);
            this.lbl2_Name.TabIndex = 4;
            this.lbl2_Name.Text = "Technician Name / Company:";
            // 
            // cb2_ReplacedBrass
            // 
            this.cb2_ReplacedBrass.AutoSize = true;
            this.cb2_ReplacedBrass.Location = new System.Drawing.Point(4, 74);
            this.cb2_ReplacedBrass.Name = "cb2_ReplacedBrass";
            this.cb2_ReplacedBrass.Size = new System.Drawing.Size(399, 18);
            this.cb2_ReplacedBrass.TabIndex = 3;
            this.cb2_ReplacedBrass.Text = "I have replaced the brass and white filters and the problem persists.";
            this.cb2_ReplacedBrass.UseVisualStyleBackColor = true;
            // 
            // cb2_InspectLine
            // 
            this.cb2_InspectLine.AutoSize = true;
            this.cb2_InspectLine.Location = new System.Drawing.Point(4, 50);
            this.cb2_InspectLine.Name = "cb2_InspectLine";
            this.cb2_InspectLine.Size = new System.Drawing.Size(425, 18);
            this.cb2_InspectLine.TabIndex = 2;
            this.cb2_InspectLine.Text = "I have visually inspected the sample line for contamination and it is clean.";
            this.cb2_InspectLine.UseVisualStyleBackColor = true;
            // 
            // cb2_InspectFilter
            // 
            this.cb2_InspectFilter.AutoSize = true;
            this.cb2_InspectFilter.Location = new System.Drawing.Point(4, 26);
            this.cb2_InspectFilter.Name = "cb2_InspectFilter";
            this.cb2_InspectFilter.Size = new System.Drawing.Size(405, 18);
            this.cb2_InspectFilter.TabIndex = 1;
            this.cb2_InspectFilter.Text = "I have visually inspected the filter for oil contamination and it is clean.";
            this.cb2_InspectFilter.UseVisualStyleBackColor = true;
            // 
            // cb2_PoweredUp
            // 
            this.cb2_PoweredUp.AutoSize = true;
            this.cb2_PoweredUp.Location = new System.Drawing.Point(4, 3);
            this.cb2_PoweredUp.Name = "cb2_PoweredUp";
            this.cb2_PoweredUp.Size = new System.Drawing.Size(426, 18);
            this.cb2_PoweredUp.TabIndex = 0;
            this.cb2_PoweredUp.Text = "I have powered up the device and confirmed that the LED is illuminated.";
            this.cb2_PoweredUp.UseVisualStyleBackColor = true;
            // 
            // lbl2_Instruct
            // 
            this.lbl2_Instruct.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl2_Instruct.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2_Instruct.Location = new System.Drawing.Point(3, 3);
            this.lbl2_Instruct.Name = "lbl2_Instruct";
            this.lbl2_Instruct.Size = new System.Drawing.Size(428, 132);
            this.lbl2_Instruct.TabIndex = 0;
            this.lbl2_Instruct.Text = resources.GetString("lbl2_Instruct.Text");
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lbl3_Instruct);
            this.tabPage3.Location = new System.Drawing.Point(4, 5);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(434, 391);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lbl3_Instruct
            // 
            this.lbl3_Instruct.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl3_Instruct.Location = new System.Drawing.Point(0, 0);
            this.lbl3_Instruct.Name = "lbl3_Instruct";
            this.lbl3_Instruct.Size = new System.Drawing.Size(434, 391);
            this.lbl3_Instruct.TabIndex = 0;
            this.lbl3_Instruct.Text = resources.GetString("lbl3_Instruct.Text");
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.lbl4_Header);
            this.tabPage4.Controls.Add(this.lbl4_Instruct);
            this.tabPage4.Location = new System.Drawing.Point(4, 5);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(434, 391);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // lbl4_Header
            // 
            this.lbl4_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl4_Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl4_Header.Location = new System.Drawing.Point(0, 0);
            this.lbl4_Header.Name = "lbl4_Header";
            this.lbl4_Header.Size = new System.Drawing.Size(434, 19);
            this.lbl4_Header.TabIndex = 1;
            this.lbl4_Header.Text = "Prepare for Air Calibration";
            this.lbl4_Header.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbl4_Instruct
            // 
            this.lbl4_Instruct.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbl4_Instruct.Location = new System.Drawing.Point(0, 31);
            this.lbl4_Instruct.Name = "lbl4_Instruct";
            this.lbl4_Instruct.Size = new System.Drawing.Size(434, 360);
            this.lbl4_Instruct.TabIndex = 0;
            this.lbl4_Instruct.Text = resources.GetString("lbl4_Instruct.Text");
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.lbl5_Header);
            this.tabPage5.Controls.Add(this.lbl5_Instruct);
            this.tabPage5.Controls.Add(this.pan5);
            this.tabPage5.Location = new System.Drawing.Point(4, 5);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(434, 391);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "tabPage5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // lbl5_Header
            // 
            this.lbl5_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl5_Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5_Header.Location = new System.Drawing.Point(0, 0);
            this.lbl5_Header.Name = "lbl5_Header";
            this.lbl5_Header.Size = new System.Drawing.Size(434, 27);
            this.lbl5_Header.TabIndex = 3;
            this.lbl5_Header.Text = "Preparing for Gas Analysis (Part 1)";
            this.lbl5_Header.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbl5_Instruct
            // 
            this.lbl5_Instruct.Location = new System.Drawing.Point(0, 27);
            this.lbl5_Instruct.Name = "lbl5_Instruct";
            this.lbl5_Instruct.Size = new System.Drawing.Size(443, 286);
            this.lbl5_Instruct.TabIndex = 2;
            this.lbl5_Instruct.Text = resources.GetString("lbl5_Instruct.Text");
            // 
            // pan5
            // 
            this.pan5.Controls.Add(this.label1);
            this.pan5.Controls.Add(this.tb5_FlowMeter);
            this.pan5.Controls.Add(this.lbl5_FlowMeter);
            this.pan5.Location = new System.Drawing.Point(0, 316);
            this.pan5.Name = "pan5";
            this.pan5.Size = new System.Drawing.Size(438, 75);
            this.pan5.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(359, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 19);
            this.label1.TabIndex = 1;
            // 
            // tb5_FlowMeter
            // 
            this.tb5_FlowMeter.Location = new System.Drawing.Point(4, 26);
            this.tb5_FlowMeter.Name = "tb5_FlowMeter";
            this.tb5_FlowMeter.Size = new System.Drawing.Size(427, 27);
            this.tb5_FlowMeter.TabIndex = 0;
            // 
            // lbl5_FlowMeter
            // 
            this.lbl5_FlowMeter.AutoSize = true;
            this.lbl5_FlowMeter.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5_FlowMeter.Location = new System.Drawing.Point(3, 9);
            this.lbl5_FlowMeter.Name = "lbl5_FlowMeter";
            this.lbl5_FlowMeter.Size = new System.Drawing.Size(170, 14);
            this.lbl5_FlowMeter.TabIndex = 0;
            this.lbl5_FlowMeter.Text = "Enter Flow Meter Reading:";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.lbl6_Header);
            this.tabPage6.Controls.Add(this.lbl6_Instruct);
            this.tabPage6.Location = new System.Drawing.Point(4, 5);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(434, 391);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "tabPage6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // lbl6_Header
            // 
            this.lbl6_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl6_Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl6_Header.Location = new System.Drawing.Point(0, 0);
            this.lbl6_Header.Name = "lbl6_Header";
            this.lbl6_Header.Size = new System.Drawing.Size(434, 30);
            this.lbl6_Header.TabIndex = 1;
            this.lbl6_Header.Text = "Preparing for Gas Analysis (Part 2)";
            this.lbl6_Header.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbl6_Instruct
            // 
            this.lbl6_Instruct.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbl6_Instruct.Location = new System.Drawing.Point(0, 30);
            this.lbl6_Instruct.Name = "lbl6_Instruct";
            this.lbl6_Instruct.Size = new System.Drawing.Size(434, 361);
            this.lbl6_Instruct.TabIndex = 0;
            this.lbl6_Instruct.Text = resources.GetString("lbl6_Instruct.Text");
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.lbl7_Header);
            this.tabPage7.Controls.Add(this.grid7_GasAnal);
            this.tabPage7.Location = new System.Drawing.Point(4, 5);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(434, 391);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "tabPage7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // lbl7_Header
            // 
            this.lbl7_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl7_Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl7_Header.Location = new System.Drawing.Point(0, 0);
            this.lbl7_Header.Name = "lbl7_Header";
            this.lbl7_Header.Size = new System.Drawing.Size(434, 26);
            this.lbl7_Header.TabIndex = 1;
            this.lbl7_Header.Text = "Gas Analysis Results";
            this.lbl7_Header.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // grid7_GasAnal
            // 
            this.grid7_GasAnal.AllowUserToAddRows = false;
            this.grid7_GasAnal.AllowUserToDeleteRows = false;
            this.grid7_GasAnal.AllowUserToResizeColumns = false;
            this.grid7_GasAnal.AllowUserToResizeRows = false;
            this.grid7_GasAnal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grid7_GasAnal.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.grid7_GasAnal.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grid7_GasAnal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grid7_GasAnal.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.grid7_GasAnal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid7_GasAnal.ColumnHeadersVisible = false;
            this.grid7_GasAnal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColProperty7,
            this.ColValue7});
            this.grid7_GasAnal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grid7_GasAnal.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grid7_GasAnal.Location = new System.Drawing.Point(0, 29);
            this.grid7_GasAnal.MultiSelect = false;
            this.grid7_GasAnal.Name = "grid7_GasAnal";
            this.grid7_GasAnal.ReadOnly = true;
            this.grid7_GasAnal.RowHeadersVisible = false;
            this.grid7_GasAnal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.grid7_GasAnal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid7_GasAnal.Size = new System.Drawing.Size(434, 362);
            this.grid7_GasAnal.TabIndex = 0;
            this.grid7_GasAnal.TabStop = false;
            // 
            // ColProperty7
            // 
            this.ColProperty7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColProperty7.DefaultCellStyle = dataGridViewCellStyle5;
            this.ColProperty7.HeaderText = "Property";
            this.ColProperty7.Name = "ColProperty7";
            this.ColProperty7.ReadOnly = true;
            this.ColProperty7.Width = 5;
            // 
            // ColValue7
            // 
            this.ColValue7.HeaderText = "Value";
            this.ColValue7.Name = "ColValue7";
            this.ColValue7.ReadOnly = true;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.lbl8_Header);
            this.tabPage8.Controls.Add(this.lbl8_Instruct);
            this.tabPage8.Location = new System.Drawing.Point(4, 5);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(434, 391);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "tabPage8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // lbl8_Header
            // 
            this.lbl8_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl8_Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl8_Header.Location = new System.Drawing.Point(0, 0);
            this.lbl8_Header.Name = "lbl8_Header";
            this.lbl8_Header.Size = new System.Drawing.Size(434, 27);
            this.lbl8_Header.TabIndex = 1;
            this.lbl8_Header.Text = "Prepare for Air Analysis";
            this.lbl8_Header.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbl8_Instruct
            // 
            this.lbl8_Instruct.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbl8_Instruct.Location = new System.Drawing.Point(0, 27);
            this.lbl8_Instruct.Name = "lbl8_Instruct";
            this.lbl8_Instruct.Size = new System.Drawing.Size(434, 364);
            this.lbl8_Instruct.TabIndex = 0;
            this.lbl8_Instruct.Text = resources.GetString("lbl8_Instruct.Text");
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.lbl9_Header);
            this.tabPage9.Controls.Add(this.grid9_AirAnal);
            this.tabPage9.Location = new System.Drawing.Point(4, 5);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(434, 391);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "tabPage9";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // lbl9_Header
            // 
            this.lbl9_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl9_Header.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl9_Header.Location = new System.Drawing.Point(0, 0);
            this.lbl9_Header.Name = "lbl9_Header";
            this.lbl9_Header.Size = new System.Drawing.Size(434, 27);
            this.lbl9_Header.TabIndex = 2;
            this.lbl9_Header.Text = "Air Analysis Results";
            this.lbl9_Header.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // grid9_AirAnal
            // 
            this.grid9_AirAnal.AllowUserToAddRows = false;
            this.grid9_AirAnal.AllowUserToDeleteRows = false;
            this.grid9_AirAnal.AllowUserToResizeColumns = false;
            this.grid9_AirAnal.AllowUserToResizeRows = false;
            this.grid9_AirAnal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grid9_AirAnal.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.grid9_AirAnal.BackgroundColor = System.Drawing.SystemColors.Window;
            this.grid9_AirAnal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grid9_AirAnal.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.grid9_AirAnal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid9_AirAnal.ColumnHeadersVisible = false;
            this.grid9_AirAnal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColProperty9,
            this.ColValue9});
            this.grid9_AirAnal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grid9_AirAnal.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grid9_AirAnal.Location = new System.Drawing.Point(0, 29);
            this.grid9_AirAnal.MultiSelect = false;
            this.grid9_AirAnal.Name = "grid9_AirAnal";
            this.grid9_AirAnal.ReadOnly = true;
            this.grid9_AirAnal.RowHeadersVisible = false;
            this.grid9_AirAnal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.grid9_AirAnal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid9_AirAnal.Size = new System.Drawing.Size(434, 362);
            this.grid9_AirAnal.TabIndex = 3;
            this.grid9_AirAnal.TabStop = false;
            // 
            // ColProperty9
            // 
            this.ColProperty9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ColProperty9.DefaultCellStyle = dataGridViewCellStyle6;
            this.ColProperty9.HeaderText = "Property";
            this.ColProperty9.Name = "ColProperty9";
            this.ColProperty9.ReadOnly = true;
            this.ColProperty9.Width = 5;
            // 
            // ColValue9
            // 
            this.ColValue9.HeaderText = "Value";
            this.ColValue9.Name = "ColValue9";
            this.ColValue9.ReadOnly = true;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.panel1);
            this.tabPage10.Location = new System.Drawing.Point(4, 5);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(434, 391);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "tabPage10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbLatestFirmware);
            this.panel1.Controls.Add(this.lblResultsHead);
            this.panel1.Controls.Add(this.lblResultsBody);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(434, 319);
            this.panel1.TabIndex = 2;
            // 
            // cbLatestFirmware
            // 
            this.cbLatestFirmware.AutoSize = true;
            this.cbLatestFirmware.Location = new System.Drawing.Point(0, 252);
            this.cbLatestFirmware.Name = "cbLatestFirmware";
            this.cbLatestFirmware.Size = new System.Drawing.Size(378, 23);
            this.cbLatestFirmware.TabIndex = 0;
            this.cbLatestFirmware.Text = "The device is already running the latest firmware.";
            this.cbLatestFirmware.UseVisualStyleBackColor = true;
            // 
            // lblResultsHead
            // 
            this.lblResultsHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblResultsHead.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResultsHead.ForeColor = System.Drawing.Color.DarkRed;
            this.lblResultsHead.Location = new System.Drawing.Point(0, 0);
            this.lblResultsHead.Name = "lblResultsHead";
            this.lblResultsHead.Size = new System.Drawing.Size(434, 57);
            this.lblResultsHead.TabIndex = 0;
            this.lblResultsHead.Text = "Diagnostics have completed, and the application found errors.";
            // 
            // lblResultsBody
            // 
            this.lblResultsBody.Location = new System.Drawing.Point(0, 75);
            this.lblResultsBody.Name = "lblResultsBody";
            this.lblResultsBody.Size = new System.Drawing.Size(434, 244);
            this.lblResultsBody.TabIndex = 1;
            this.lblResultsBody.Text = resources.GetString("lblResultsBody.Text");
            // 
            // progBarPage
            // 
            this.progBarPage.Location = new System.Drawing.Point(93, 406);
            this.progBarPage.MarqueeAnimationSpeed = 0;
            this.progBarPage.Name = "progBarPage";
            this.progBarPage.Size = new System.Drawing.Size(438, 23);
            this.progBarPage.TabIndex = 4;
            this.progBarPage.Value = 4;
            // 
            // lblPage
            // 
            this.lblPage.AutoSize = true;
            this.lblPage.Location = new System.Drawing.Point(302, 411);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(56, 13);
            this.lblPage.TabIndex = 4;
            this.lblPage.Text = "Page 5/10";
            this.lblPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.Location = new System.Drawing.Point(13, 387);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(45, 13);
            this.lblVersion.TabIndex = 4;
            this.lblVersion.Text = "Version ";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(624, 441);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblPage);
            this.Controls.Add(this.progBarPage);
            this.Controls.Add(this.imgBrandLogo);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrev);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "R1234yf Embedded";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgBrandLogo)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.pan2.ResumeLayout(false);
            this.pan2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.pan5.ResumeLayout(false);
            this.pan5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid7_GasAnal)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid9_AirAnal)).EndInit();
            this.tabPage10.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label lbl1_Instruct;
        private System.Windows.Forms.PictureBox imgBrandLogo;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.ProgressBar progBarPage;
        private System.Windows.Forms.Label lblPage;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lbl2_Instruct;
        private System.Windows.Forms.Panel pan2;
        private System.Windows.Forms.CheckBox cb2_PoweredUp;
        private System.Windows.Forms.CheckBox cb2_ReplacedBrass;
        private System.Windows.Forms.CheckBox cb2_InspectLine;
        private System.Windows.Forms.CheckBox cb2_InspectFilter;
        private System.Windows.Forms.Label lbl2_TechnicianNotes;
        private System.Windows.Forms.Label lbl2_DeviceSerNo;
        private System.Windows.Forms.Label lbl2_Name;
        private System.Windows.Forms.TextBox tb2_Technician;
        private System.Windows.Forms.TextBox tb2_SerialNo;
        private System.Windows.Forms.TextBox tb2_Notes;
        private System.Windows.Forms.Label lbl3_Instruct;
        private System.Windows.Forms.Label lbl4_Instruct;
        private System.Windows.Forms.Label lbl5_Instruct;
        private System.Windows.Forms.Panel pan5;
        private System.Windows.Forms.Label lbl5_FlowMeter;
        private System.Windows.Forms.TextBox tb5_FlowMeter;
        private System.Windows.Forms.Label lbl6_Instruct;
        private System.Windows.Forms.DataGridView grid7_GasAnal;
        private System.Windows.Forms.Label lbl8_Instruct;
        private System.Windows.Forms.Label lblResultsHead;
        private System.Windows.Forms.CheckBox cbLatestFirmware;
        private System.Windows.Forms.Label lbl4_Header;
        private System.Windows.Forms.Label lbl1_Header;
        private System.Windows.Forms.Label lbl5_Header;
        private System.Windows.Forms.Label lbl6_Header;
        private System.Windows.Forms.Label lbl7_Header;
        private System.Windows.Forms.Label lbl8_Header;
        private System.Windows.Forms.Label lbl9_Header;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColProperty7;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColValue7;
        private System.Windows.Forms.DataGridView grid9_AirAnal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColProperty9;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColValue9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblResultsBody;
    }
}

