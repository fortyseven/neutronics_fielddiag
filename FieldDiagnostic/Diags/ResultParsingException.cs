﻿using System;
using System.Runtime.Serialization;

namespace FieldDiagnostic.Diags
{
    [Serializable]
    public class ResultParsingException : Exception
    {
        public ResultParsingException()
        {
        }

        public ResultParsingException( string message )
            : base( message )
        {
        }

        public ResultParsingException( string message, Exception inner )
            : base( message, inner )
        {
        }

        protected ResultParsingException(
            SerializationInfo info,
            StreamingContext context )
            : base( info, context )
        {
        }
    }
}