﻿using FieldDiagnostic.Device;

namespace FieldDiagnostic.Diags
{
    public struct Results
    {
        #region ResultCodes
        private const string RESULT_CODE_VDA_PASS                = "00000";
        private const string RESULT_CODE_VDA_FAIL                = "99999";

        public const string RESULT_CODE_SAE_UNSTABLE_READING        = "00001";
        public const string RESULT_CODE_SAE_EXCESSIVE_READING_VALUE = "00002";
        public const string RESULT_CODE_SAE_LOW_OUTPUT_CALIBRATION  = "00003";
        public const string RESULT_CODE_SAE_TEMP_OUT_OF_RANGE       = "00004";
        public const string RESULT_CODE_SAE_EXCESSIVE_AIR           = "00005";
        #endregion

        private const int EXPECTED_ARG_COUNT_VDA = 2;
        private const int EXPECTED_ARG_COUNT_SAE = 7;

        //private const int ANALYSIS_RESULT_LENGTH_SAE  = 41;

        private string _raw;

        #region Properties

        public string R134a { get; private set; }
        public string R1234yf { get; private set; }
        public string R22 { get; private set; }
        public string HC { get; private set; }
        public string Air { get; private set; }
        public string ResultCode { get; private set; }
        public string ReadingNumber { get; private set; }

        /// <summary>
        /// Setting value will trigger validation; throws exception if parsing fails.
        /// </summary>
        public string Raw
        {
            get
            {
                return _raw;
            }
            set
            {
                _raw = value;
                ParseRaw();
            }
        }
        #endregion

        /*************************************************************************/
        /// <summary>
        /// Parse data freshly placed into _raw; data as 0520/0530 device unless otherwise specified.
        /// </summary>
        private void ParseRaw()
        {
            if ( MainForm.State.Device.DeviceType == AnalyzerDevice.Type.VDA ) {
                ParseVDA();
                return;
            }

            // By default, parse for the 520/530 series

            //if ( _raw.Length != ANALYSIS_RESULT_LENGTH_SAE ) {
            //    Clear();
            //    throw new ResultParsingException( "Invalid analysis data" );
            //}
            string[] values = _raw.Split( ' ' );
            if ( values.Length == EXPECTED_ARG_COUNT_SAE ) {
                R134a = values[ 0 ];
                R1234yf = values[ 1 ];
                R22 = values[ 2 ];
                HC = values[ 3 ];
                Air = values[ 4 ];
                ResultCode = values[ 5 ];
                ReadingNumber = values[ 6 ];
            }
            else {
                string msg = "Invalid count of arguments encountered (" + values.Length + "). Data received: '" + _raw + "'";
                Clear();
                throw new ResultParsingException( msg );
            }
        }

        /*************************************************************************/
        /// <summary>
        /// Raw data is from 0490/0510 VDA device; parse accordingly.
        /// </summary>
        private void ParseVDA()
        {
            string[] values = _raw.Split( ' ' );

            if ( values.Length == EXPECTED_ARG_COUNT_VDA ) {
                // These are dubious, but...
                R134a = "000.0";
                R22 = "000.0";
                HC = "000.0";

                ResultCode = values[ 0 ];

                R1234yf = ( ResultCode.Equals( RESULT_CODE_VDA_PASS ) ? "100.0" : "000.0" );
                Air = ( ResultCode.Equals( RESULT_CODE_VDA_FAIL ) ? "000.0" : "100.0" );

                ReadingNumber = values[ 1 ];
            }
            else {
                string msg = "Invalid count of arguments for 0490 device encountered (" + values.Length + "). Data received: '" + _raw + "'";
                Clear();
                throw new ResultParsingException( msg );
            }
        }

        /*************************************************************************/
        /// <summary>
        /// Reset all values to initial, blank state.
        /// </summary>
        public void Clear()
        {
            R134a = null;
            R1234yf = null;
            R22 = null;
            HC = null;
            Air = null;
            ResultCode = null;
            ReadingNumber = null;
            _raw = null;
        }

        /*************************************************************************/
        /// <summary>
        /// For debugging only.
        /// </summary>
        /// <returns>String representation of structure.</returns>
        public override string ToString()
        {
            if ( _raw != null )
                return "R134a = " + R134a +
                       ", R1234yf = " + R1234yf +
                       ", R22 = " + R22 +
                       ", HC = " + HC +
                       ", Air = " + Air +
                       ", ResultCode = " + ResultCode +
                       ", ReadingNumber = " + ReadingNumber;

            return "No data";
        }
    }
}
