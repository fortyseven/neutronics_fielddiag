﻿using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using FieldDiagnostic.Device;
using FieldDiagnostic.Pages.UI;

namespace FieldDiagnostic.Diags
{

    public class DeviceDetection
    {
        public delegate void OnFinishCallback( AnalyzerDevice device );

        private const int       MAX_RETRIES     = 5;

        private OnFinishCallback    _cb_on_finish;
        private AnalyzerDevice    _device;
        private StringBuilder _errors;
        bool _found_device;
        public bool WasCanceled { get; private set; }

        /*************************************************************************/
        public void BeginDetectionThread()
        {
            if ( _cb_on_finish == null ) {
                throw new Exception( "No OnFinish callback assigned" );
            }
            Thread thread = new Thread( DetectionThread );
            thread.Start();
            WasCanceled = false;
            _found_device = false;
            _errors = new StringBuilder();
        }

        /*************************************************************************/
        private void DetectionThread()
        {
            Thread.CurrentThread.Name = "Detection Thread";
            Debug.WriteLine( "DetectionThread Start" );

            if ( SerialPort.GetPortNames().Length == 0 ) {
                _errors.Append( "No serial ports found." );
                BusyForm.Dismiss();
                _cb_on_finish( null );
                return;
            }
#if DEBUG
            Debug.WriteLine( "PORTS REPORTED:" );
            foreach ( string name in SerialPort.GetPortNames() ) {
                Debug.WriteLine( "> " + name );
            }
#endif
            // Enumerate devices and attempt to find a device on each one
            foreach ( string name in SerialPort.GetPortNames() ) {
                try {
                    if ( WasCanceled ) {
                        _cb_on_finish( null );
                        return;
                    }

                    BusyForm.SetCaption( "Attempting " + name + "..." );

                    _device = new AnalyzerDevice( name );
                    //_device = new AnalyzerDeviceTEST_SAE_Good(null);
                    //_device = new AnalyzerDeviceTEST_VDA_Good( null );

                    _found_device = PerformDetectionOnDevice();

                    // Did we find an identifier? We're done here. Pass the
                    // still OPEN serial port back to the caller
                    if ( _found_device )
                        break;

                    _device.Done();

                    Application.DoEvents();
                    Thread.Sleep( 250 );
                }
                catch ( Exception e ) {
                    _errors.Append( "   - [Port " + name + "]" + e.Message + "\n" );
                }
            }

            if ( !_found_device ) {
                _device = null;
                _errors.Append( "No devices found." );
            }

            _cb_on_finish( _device );
        }

        /*************************************************************************/
        public string GetErrors()
        {
            return _errors.ToString();
        }

        /*************************************************************************/
        private bool PerformDetectionOnDevice()
        {
            int retry_count = 0;
            int found_and_initializing_bonus_time = 0;
            bool found_device = false;

            while ( retry_count < MAX_RETRIES + found_and_initializing_bonus_time ) {
                if ( WasCanceled )
                    break;
                // Should block (we're on a thread, right?) until we get a result or a timeout
                switch ( _device.CmdSystemCheck() ) {
                    case AnalyzerDevice.Response.F_REPLACEFEILTER:
                        _device.CmdResetFilterCounter();
                        BusyForm.SetCaption( found_device
                            ? "Device ready! (Had to reset filter counter.)"
                            : "Found identifier! (Had to reset filter counter.)" );
                        found_device = true;

                        // Force the retry loop to finish
                        retry_count = MAX_RETRIES + found_and_initializing_bonus_time;
                        break;
                    case AnalyzerDevice.Response.T_TANKPURGEMODE:
                    // TODO: (Future use; ignore for now)
                    case AnalyzerDevice.Response.Q_REQCALIBRATION:
                    case AnalyzerDevice.Response.ACK:
                        BusyForm.SetCaption( found_device ? "Device ready!" : "Found identifier!" );
                        found_device = true;

                        // Force the retry loop to finish
                        retry_count = MAX_RETRIES + found_and_initializing_bonus_time;
                        break;
                    case AnalyzerDevice.Response.NAK:
                        if ( !found_device ) {
                            BusyForm.SetCaption( "Found identifier! Waiting for device to be ready." );
                            found_device = true;
                            found_and_initializing_bonus_time = 10;
                            retry_count = 0;
                        }
                        Thread.Sleep( 2000 );
                        break;
                    case AnalyzerDevice.Response.ERROR:
                        BusyForm.SetCaption( "Unrecognized response; skipping." );
                        break;
                    case AnalyzerDevice.Response.TIMEDOUT:
                        BusyForm.SetCaption( "Timed out waiting for a response. (Try " + ( retry_count + 1 ) + "/" + MAX_RETRIES + ")" );
                        break;
                }
                retry_count++;
            }
            return found_device;
        }

        /*************************************************************************/
        public void SetOnFinishCallback( OnFinishCallback callback )
        {
            _cb_on_finish = callback;
        }

        /*************************************************************************/
        internal void Cancel()
        {
            Thread.CurrentThread.Interrupt();
            WasCanceled = true;
        }
    }
}