﻿using System;
using System.Drawing;
using System.Windows.Forms;
using FieldDiagnostic.Pages;

namespace FieldDiagnostic
{
    public partial class MainForm : Form
    {
        private const string APP_TITLE = "R1234yf Field Diagnostic Tool SAE";

        public static AppState State;

        private WizardController _wizard;

        enum STEPS
        {
            STEP01_INTRO = 0,
            STEP02_VISUALINSPECTION,
            STEP03_INITIALIZINGIDENTIFIER,
            STEP04_PREPAREAIRCALIBRATION,
            STEP05_GASFLOWVERIFICATION,
            STEP06_GASANALYSIS,
            STEP07_RESULTS,
            STEP08_PREPAREAIRANALYSIS,
            STEP09_RESULTS,
            STEP10_ERRORS
        };

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load( object sender, EventArgs e )
        {
            State = new AppState();

#if DEBUG
            this.Text = APP_TITLE + " v" + Application.ProductVersion + " DEBUG BUILD";
            lblVersion.Text = "Version " + Application.ProductVersion + " DEBUG";
#else
            this.Text = APP_TITLE + " v" + Application.ProductVersion;
            lblVersion.Text = "Version " + Application.ProductVersion;
#endif

            tabControl1.ItemSize = new Size( 0, 1 );
            tabControl1.Appearance = TabAppearance.FlatButtons;
            tabControl1.SizeMode = TabSizeMode.Fixed;

            _wizard = new WizardController( this );

            _wizard.RegisterPage( (int)STEPS.STEP01_INTRO, new WP_Page01_Welcome( _wizard ) );
            _wizard.RegisterPage( (int)STEPS.STEP02_VISUALINSPECTION, new WP_Page02_InspectionInfo( _wizard ) );
            _wizard.RegisterPage( (int)STEPS.STEP03_INITIALIZINGIDENTIFIER, new WP_Page03_Detection( _wizard ) );
            _wizard.RegisterPage( (int)STEPS.STEP04_PREPAREAIRCALIBRATION, new WP_Page04_AirCalibration( _wizard ) );
            _wizard.RegisterPage( (int)STEPS.STEP05_GASFLOWVERIFICATION, new WP_Page05_FlowMeterReading( _wizard ) );
            _wizard.RegisterPage( (int)STEPS.STEP06_GASANALYSIS, new WP_Page06_GasAnalysis( _wizard ) );
            _wizard.RegisterPage( (int)STEPS.STEP07_RESULTS, new WP_Page07_GasAnalysisResults( _wizard ) );
            _wizard.RegisterPage( (int)STEPS.STEP08_PREPAREAIRANALYSIS, new WP_Page08_AirAnalysis( _wizard ) );
            _wizard.RegisterPage( (int)STEPS.STEP09_RESULTS, new WP_Page09_AirAnalysisResults( _wizard ) );
            _wizard.RegisterPage( (int)STEPS.STEP10_ERRORS, new WP_Page10_Finish( _wizard ) );

            _wizard.Reset();



            //_wizard.SetPage( 10 - 1 );
        }

        private void btnNext_Click( object sender, EventArgs e )
        {
            if ( _wizard.CurrentPage == ( 10 - 1 ) ) {
                if (State.IdentifierDetectionFailed) {
                    Application.Exit();
                }
                // Try to save a report
                MainForm.State.Save();
                if ( State.WasSaved )
                    Application.Exit();
            }
            else {
                _wizard.Next();
            }
        }

        private void btnPrev_Click( object sender, EventArgs e )
        {
            _wizard.Prev();
        }

        private void MainForm_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( !State.WasSaved && !State.IdentifierDetectionFailed ) {
                if ( MessageBox.Show( "Quit without finishing diagnostic?",
                                         "Quit?",
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question ) == DialogResult.No ) {
                    e.Cancel = true;
                    return;
                }
            }
            //System.Environment.Exit( 1 );

            _wizard.Exit( true );
        }
    }
}