﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using FieldDiagnostic.Pages.UI;

namespace FieldDiagnostic
{
    class WizardController
    {
        private readonly Form            _parent_form;
        private readonly TabControl      _tab_control;
        private readonly Label           _pagelabel;
        private readonly ProgressBar     _progbar;
        private readonly Button          _btn_prev;
        private readonly Button          _btn_next;
        private readonly IWizardPage[]   _pages;

        public int CurrentPage { get; private set; }

        #region properties
        int PageCount { get { return _tab_control.TabCount; } }
        #endregion

        public WizardController( Form form )
        {
            _parent_form = form;

            CurrentPage = 0;

            _tab_control = form.Controls[ "tabControl1" ] as TabControl;

            _progbar = form.Controls[ "progBarPage" ] as ProgressBar;
            if ( _progbar == null )
                throw new Exception( "Progbar was null; this shouldn't happen." );
            _progbar.Maximum = PageCount - 1;
            _progbar.Value = CurrentPage;

            _pagelabel = form.Controls[ "lblPage" ] as Label;

            _btn_prev = form.Controls[ "btnPrev" ] as Button;
            _btn_next = form.Controls[ "btnNext" ] as Button;

            _pages = new IWizardPage[ PageCount ];
            for ( var i = 0; i < PageCount; i++ ) {  //FIXME: Does C# initialize to null? Is this redundant?
                _pages[ i ] = null;
            }

            Reset();
        }

        /// <summary>
        /// Directly navigate to a specific page
        /// </summary>
        public void SetPage( int index, bool skip_onexit = false )
        {
            if ( _pages[ CurrentPage ] == null || _pages[ index ] == null )
                return;

            _pages[ CurrentPage ].OnPageChange();

            if ( ( !skip_onexit ) && !_pages[ CurrentPage ].OnPageExit() ) {
                // Page is not allowing us to change
                return;
            }
            CurrentPage = index;

            // Update progress bar position
            _progbar.SynchronizedInvoke( () => _progbar.Value = index );

            // Update page text on progress bar
            _pagelabel.SynchronizedInvoke( () => _pagelabel.Text = "Page " + ( index + 1 ).ToString() + "/" + PageCount.ToString() );

            // Flip to the appropriate tab
            _tab_control.SynchronizedInvoke( () => _tab_control.SelectTab( index ) );

            if ( _pages[ index ] != null ) {
                _pages[ index ].OnPageEnter();
            }
        }

        /// <summary>
        /// Navigate to the next tab
        /// </summary>
        public void Next()
        {
            //_page_prev = CurrentPage;
            if ( CurrentPage < PageCount - 1 ) {
                SetPage( CurrentPage + 1 );
            }
            //else {
            //    // Last page; done, exit
            //    Exit();
            //}
        }

        /// <summary>
        /// Force navigation to the next page, regardless of button state
        /// </summary>
        public void NextForce()
        {
            if ( CurrentPage < PageCount - 1 ) {
                SetPage( CurrentPage + 1, true );
            }
        }

        /// <summary>
        /// Navigate to the previous tab
        /// </summary>
        public void Prev()
        {
            if ( CurrentPage <= 0 )
                return;

            SetPage( CurrentPage - 1, true );
        }

        public void Reset()
        {
            SetPage( 0 );
        }

        public void UnlockNext() { _btn_next.SynchronizedInvoke( () => _btn_next.Enabled = true ); }
        public void UnlockPrev() { _btn_prev.SynchronizedInvoke( () => _btn_prev.Enabled = true ); }
        public void LockNext() { _btn_next.SynchronizedInvoke( () => _btn_next.Enabled = false ); }
        public void LockPrev() { _btn_prev.SynchronizedInvoke( () => _btn_prev.Enabled = false ); }

        public void RegisterPage( int index, IWizardPage page )
        {
            _pages[ index ] = page;
            page.SetParentForm( _parent_form );
        }

        //public void OnNextClick()
        //{
        //    _pages[ CurrentPage ].Stop();
        //}

        public void Exit( bool skip_onexit = false )
        {
            Debug.WriteLine( "Exit called; thread stop called" );
            
            if ( MainForm.State.Device != null )
                MainForm.State.Device.Done();
            
            BusyForm.Dismiss();
            
            _pages[ CurrentPage ].Stop();
            
            if ( !skip_onexit ) {
                _pages[ CurrentPage ].OnExit();
            }
        }
    }
}
