﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO.Ports;
using System.Threading;
using FieldDiagnostic.Diags;
// ReSharper disable UnusedMember.Local

namespace FieldDiagnostic.Device
{
    public class AnalyzerDevice
    {
        private const int       SERIAL_BAUD             = 115200;
        private const Parity    SERIAL_PARITY           = Parity.None;
        private const int       SERIAL_BITS             = 8;
        private const StopBits  SERIAL_STOPBITS         = StopBits.One;

        private const string    NEWLINE                 = "\x0d";

        private const int       RESPONSE_TIMEOUT_MS     = 2000;
        //private const int       RESPONSE_TIMEOUT_MS     = 10;
        private const int       MAX_LAST_RESULTS        = 5;

        public enum Type
        {
            SAE,
            VDA
        }

        private static class Command
        {
            public const string SYSTEM_CHECK                    = "N";
            public const string CALIBRATE_NOW                   = "C";
            public const string GET_DEVICE_NAME                 = "D";
            public const string GET_SOFTWARE_REV                = "G";
            public const string GET_SERIAL_NUMBER               = "B";
            public const string GET_FIRMWARE_BUILD              = "!";
            public const string LAST_FIVE_RESULTS               = "L";
            public const string RESET_FILTER_COUNT              = "R";
            public const string LAST_FIVE_RESULTS_UNENCRYPTED   = "V";
            public const string GAS_ANALYSIS                    = "A";
            public const string GAS_ANALYSIS_UNENCRYPTED        = "U";
            public const string VERBOSE                         = "X"; // "display more teXt"; human readable commands + data

            public const int GET_DEVICE_NAME_LEN   = 5;
            public const int GET_SOFTWARE_REV_LEN  = 5;
            public const int GET_SERIAL_NUMBER_LEN = 8;
        }

        [SuppressMessage( "ReSharper", "UnusedMember.Global" )]
        public static class Response
        {
            public const byte ERROR                = 0;                // unknown response, or timed out
            public const byte ACK                  = 0x06;
            public const byte NAK                  = 0x15;

            public const byte Q_REQCALIBRATION     = (byte)'Q';       // Initialized; but request calibration
            public const byte T_TANKPURGEMODE      = (byte)'T';       // Intiailized; but in tank purge mode (future use)
            public const byte F_REPLACEFEILTER     = (byte)'F';       // Initialized; but require filter replacement (150 tests)
            public const byte O_AIRFILTERWARNING   = (byte)'O';       // Intiailized; but air filter will expire soon

            public const byte o_AIRSENSORWARNSUPR  = (byte)'o';       // Air filter warning will be suppressed for five readings
            public const byte a_ANALYZEOK          = (byte)'a';       // Beginning analysis (NAK if not ready)
            public const byte c_CALIBRATEOK        = (byte)'c';       // Beginning calibration; will return ACK on completion
            public const byte r_FILTERCOUNTRESETOK = (byte)'r';       // Filter counter has been reset

            public const byte l_LASTFIVERESULTS    = (byte)'l';       // Last five results; separated by CR
            public const byte v_LASTFIVERESULTS_UN = (byte)'v';       // Last five results, unencrypted; separated by CR

            public const byte a_GAS_ANALYSIS       = (byte)'a';       // Performing gas analysis
            public const byte u_GAS_ANALYSIS_UN    = (byte)'u';       // Performing gas analysis, unencrypted

            public const byte d_DEVICENAME         = (byte)'d';       // Device name follows (d####+CR) (e.g. d0353)
            public const byte g_SOFTWARE_REV       = (byte)'g';       // Software revision number follows (g####+CR) (e.g. g0100; Major 01; Minor 00)
            public const byte b_SERIAL_NUMBER      = (byte)'b';       // Serial number follows (b#######+CR) (e.g. b0012345)

            public const byte x_VERBOSEENABLED     = (byte)'x';       // Device is in "display more teXt" mode

            public const byte TIMEDOUT             = 254;
            public const byte UNKNOWN              = 255;
        }

        //public struct ResponseData
        //{
        //    public int      response_code;
        //    public string   args;
        //    public string   raw;
        //}

        protected readonly string _com_port_name;
        protected SerialPort      _serial;
        protected string          _device_name = "";
        protected string          _software_revision = "";
        protected string          _serial_number = "";
        protected string          _firmware_build = "";
        protected List<string>    _last_results;

        #region Properties
        public string PortName
        {
            get { return _com_port_name; }
        }
        public string DeviceName
        {
            get
            {
                if ( _device_name.Equals( "" ) ) {
                    throw new Exception( "GatherDeviceDetails has not yet been called." );
                }
                return _device_name;
            }
            internal set { _device_name = value; }
        }

        public Type DeviceType { get; protected set; }
        public string SoftwareRevision
        {
            get
            {
                if ( _software_revision.Equals( "" ) ) {
                    throw new Exception( "GatherDeviceDetails has not yet been called." );
                }
                return _software_revision;
            }
            internal set { _software_revision = value; }
        }
        public string SerialNumber
        {
            get
            {
                if ( _serial_number.Equals( "" ) ) {
                    throw new Exception( "GatherDeviceDetails has not yet been called." );
                }
                return _serial_number;
            }
            internal set { _serial_number = value; }
        }
        public string FirmwareBuild
        {
            get
            {
                if ( _firmware_build.Equals( "" ) ) {
                    throw new Exception( "GatherDeviceDetails has not yet been called." );
                }
                return _firmware_build;
            }
            internal set { _firmware_build = value; }
        }
        public List<string> LastResults
        {
            get
            {
                if ( _last_results == null ) {
                    throw new Exception( "GatherDeviceDetails has not yet been called." );
                }
                return _last_results;
            }
            internal set { _last_results = value; }
        }

        #endregion

        //public delegate void OnDeviceResponseFunc( ResponseData data );
        //public OnDeviceResponseFunc OnDeviceResponse = null;

        /*************************************************************************/
        /// <summary>
        /// Opens a serial interface to the identifier on the com port specified.
        /// </summary>
        /// <param name="com_port">COM port identifier (e.g. 'COM4', 'COM7').</param>
        public AnalyzerDevice( string com_port )
        {
            if ( com_port.Equals( "DEBUG" ) ) {
                Debug.WriteLine( "USING DEBUG DEVICE" );
                return;
            }

            _com_port_name = com_port;

            _serial = new SerialPort( _com_port_name, SERIAL_BAUD, SERIAL_PARITY, SERIAL_BITS, SERIAL_STOPBITS ) {
                ReadTimeout = RESPONSE_TIMEOUT_MS,
                WriteTimeout = RESPONSE_TIMEOUT_MS,
                NewLine = NEWLINE
            };

            _serial.Open();
        }

        /*************************************************************************/
        /// <summary>
        /// Closes the serial port, if it's open.
        /// </summary>
        public virtual void Done()
        {
            if ( ( _serial != null ) && ( _serial.IsOpen ) ) {
                try {
                    _serial.Close();
                }
                catch {
                    //Don't care
                }
            }
            _serial = null;
        }

        /*************************************************************************/
        /// <summary>
        /// Sends the system check command; used during detection and initialzation.
        /// </summary>
        /// <returns>Response code; typically F, T, Q, {ACK}, and {NAK}.</returns>
        public virtual int CmdSystemCheck()
        {
            /* 
             * Responses: 
             *  ACK - System OK
             *    Q - Request Calibration
             *    T - (Future Use) In Tank Purge Mode
             *    F - Replace Filter (after 150 tests)
             */

            int response_code;

            _serial.DiscardInBuffer();

            try {
                _serial.Write( Command.SYSTEM_CHECK );
                response_code = _serial.ReadByte();
            }
            catch ( TimeoutException ) {
                response_code = Response.TIMEDOUT;
            }
            catch ( Exception ) {
                response_code = Response.ERROR;
            }

            return response_code;
        }

        /*************************************************************************/
        public virtual int CmdResetFilterCounter()
        {
            int response_code = Response.ERROR;

            try {
                _serial.Write( Command.RESET_FILTER_COUNT );
                _serial.DiscardInBuffer(); // let's just pretend...
            }
            catch ( TimeoutException ) {
                response_code = Response.TIMEDOUT;
            }
            catch ( Exception ) {
                response_code = Response.ERROR;
            }

            return response_code;
        }

        /*************************************************************************/
        /// <summary>
        /// Performs a series of queries retrieving the most important information about the identifier.
        /// </summary>
        public virtual void GatherDeviceDetails()
        {
            _serial.DiscardInBuffer();
            _device_name = CmdGetStringResponse( Command.GET_DEVICE_NAME, (char)Response.d_DEVICENAME, Command.GET_DEVICE_NAME_LEN );

            if ( _device_name.Equals( "0490" ) || _device_name.Equals( "0510" ) ) {
                DeviceType = Type.VDA;
            }
            else {
                DeviceType = Type.SAE;
            }

            _software_revision = CmdGetStringResponse( Command.GET_SOFTWARE_REV, (char)Response.g_SOFTWARE_REV, Command.GET_SOFTWARE_REV_LEN );
            _serial_number = CmdGetStringResponse( Command.GET_SERIAL_NUMBER, (char)Response.b_SERIAL_NUMBER, Command.GET_SERIAL_NUMBER_LEN );
            _firmware_build = CmdGetFirmwareBuild();
            _last_results = CmdGetLastResults();
        }

        /*************************************************************************/
        /// <summary>
        /// Queries the identifier about it's firmware build number.
        /// </summary>
        /// <returns>Firmware build number.</returns>
        private string CmdGetFirmwareBuild()
        {
            try {
                _serial.Write( Command.GET_FIRMWARE_BUILD );
                return _serial.ReadLine().Trim();
            }
            catch ( TimeoutException ) {
                throw new Exception( "Timed out fetching firmware build" );
            }
        }
        /*************************************************************************/
        /// <summary>
        /// A generic command query where a response of a single character, and string of a specific length is expected.
        /// </summary>
        /// <param name="command_code">Single character command code (e.g. 'E').</param>
        /// <param name="expected_response">Single character command response (e.g. 'e').</param>
        /// <param name="expected_length">Length of string expected after command response.</param>
        /// <returns>Content of string after command response.</returns>
        private string CmdGetStringResponse( string command_code, char expected_response, int expected_length )
        {
            string response;

            try {
                _serial.DiscardInBuffer();
                _serial.Write( command_code );

                response = _serial.ReadLine();

                if ( response.Length != expected_length ) {
                    throw new Exception( "Received inappropriate response for [" + command_code + "]; expected " + expected_length + " chars, received " + response.Length + " (\"" + response + "\")" );
                }

                if ( response[ 0 ] == expected_response ) {
                    response = response.Substring( 1 );
                }
                else {
                    throw new Exception( "Received unknown response to query: " + response );
                }
            }
            catch ( TimeoutException ) {
                throw new Exception( "Timed out waiting for response to query" );
            }

            return response;
        }
        /*************************************************************************/
        /// <summary>
        /// Queries the identifier to return, at most, the last 5 analysis results. 
        /// </summary>
        /// <returns>List of strings containing each line of the device response.</returns>
        public virtual List<string> CmdGetLastResults()
        {
            // This is written to allow for variable numbers of responses; it keeps checking
            // to see if there's another full line from the identifier. If it times out, we're
            // probably done. This was done to keep it from being hard coded to a particular
            // limit. 
            //
            // We're not trying to pass the results through a Results object for parisng, we're
            // just quietly recording each line raw.

            List<string> results = new List<string>( 0 );
            int result_count = 0;
            _serial.ReadTimeout = 5000;
            _serial.Write( Command.LAST_FIVE_RESULTS_UNENCRYPTED );

            try {
                int cmd_resp = _serial.ReadChar();
                if ( cmd_resp != Response.v_LASTFIVERESULTS_UN ) {
                    throw new Exception( "Unexpected response to previous results query" );
                }
                while ( result_count < MAX_LAST_RESULTS ) {
                    string response = _serial.ReadTo( "\r" );
                    if ( response.Trim().Length > 0 ) {
                        results.Add( response );
                    }
                    result_count++;
                }
            }
            catch ( TimeoutException ) {
                // Nothing more to say, apparently.
                return results;
            }
            finally {
                _serial.ReadTimeout = RESPONSE_TIMEOUT_MS;
            }
            return results;
        }

        /*************************************************************************/
        /// <summary>
        /// Performs calibration procedure. Please insert your ear plugs.
        /// </summary>
        /// <returns></returns>
        public virtual void CmdDoCalibration()
        {
            _serial.DiscardInBuffer();

            try {
                _serial.Write( Command.CALIBRATE_NOW ); // WHIIIIIIIRRR!!

                while ( _serial.BytesToRead < 1 ) {
                    Thread.Sleep( 150 );
                }
                int resp = _serial.ReadChar();
                if ( resp != Response.c_CALIBRATEOK ) {
                    throw new Exception( "Did not get expected response. Aborting. (Received '" + resp + "')" );
                }

                // Calibration takes a while, so wait a bit.
                _serial.ReadTimeout = 1000 * 42;

                try {
                    //FIXME: Depending on the timeout, here...this might be fine.
                    resp = _serial.ReadChar();

                    if ( resp != Response.ACK ) {
                        throw new Exception( "Returned something other than ACK: " + Convert.ToByte( resp ) );
                    }

                    // We'll fall through, and out of the method if we're successful, like a magical water slide.
                }
                catch ( TimeoutException ) {
                    throw new Exception( "Device response timeout" );
                }
            }
            finally {
                // Reset timeout
                _serial.ReadTimeout = RESPONSE_TIMEOUT_MS;
            }
        }

        /*************************************************************************/
        /// <summary>
        /// Performs the gas analysis procedure.
        /// </summary>
        /// <returns>Raw contents of analysis result.</returns>
        public virtual string CmdDoAnalysis()
        {
            string analysis_result;

            try {
                _serial.DiscardInBuffer();

                _serial.Write( Command.GAS_ANALYSIS_UNENCRYPTED );
                while ( _serial.BytesToRead < 1 ) {
                    Thread.Sleep( 150 );
                }
                int resp = _serial.ReadChar();

                if ( resp != Response.u_GAS_ANALYSIS_UN ) {
                    throw new Exception( "Did not get expected response to analysis request: (Received ASCII '" + resp + "')" );
                }

                // Analysis takes a while, so wait a bit.
                _serial.ReadTimeout = 1000 * 90;

                try {
                    analysis_result = _serial.ReadLine();
                    Results test_results = new Results();
                    try {
                        test_results.Raw = analysis_result;
                    }
                    catch ( ResultParsingException e ) {
                        throw new Exception( "There was a problem understanding the analysis response. " + e.Message );
                    }
                }
                catch ( TimeoutException ) {
                    throw new Exception( "Timed out waiting for ACK." );
                }
            }
            finally {
                // Acknowldege "Displaying Results" state
                _serial.Write( "N" );

                // Reset timeout
                _serial.ReadTimeout = RESPONSE_TIMEOUT_MS;
            }

            // TODO: Why aren't we just returning the Result object? We did the work already? Leaving it for now; don't feel like potentially breaking something right now.

            return analysis_result;
        }

        /*************************************************************************/
        public override string ToString()
        {
            return "[ Name: " + DeviceName + " SRev: " + SoftwareRevision + " SerNo: " + SerialNumber + " ] on " + PortName;
        }
    }
}
