﻿namespace FieldDiagnostic.Device.Test
{
    public class AnalyzerDeviceTEST_VDA_Good : AnalyzerDeviceTEST
    {
        public AnalyzerDeviceTEST_VDA_Good( string com_port )
            : base( com_port )
        {

        }
        /*************************************************************************/
        public override void GatherDeviceDetails()
        {
            base.GatherDeviceDetails();
            DeviceName = "0490";
            DeviceType = Type.VDA;
        }

        /*************************************************************************/
        private int _c = 0;
        public override string CmdDoAnalysis()
        {
            base.CmdDoAnalysis();
            return ( _c++ % 2 == 0 ) ? "00000 47474" : "00005 47474";
        }

        /*************************************************************************/
        public override string ToString()
        {
            return "TEST VDA OBJECT (GOOD) " + "[ Name: " + DeviceName + " SRev: " + SoftwareRevision + " SerNo: " + SerialNumber + " Firm: " + FirmwareBuild + "]";
        }
    }
} ;
