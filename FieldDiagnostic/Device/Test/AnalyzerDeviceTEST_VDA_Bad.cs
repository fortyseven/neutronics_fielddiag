﻿namespace FieldDiagnostic.Device.Test
{
    public class AnalyzerDeviceTEST_VDA_Bad : AnalyzerDeviceTEST
    {
        public AnalyzerDeviceTEST_VDA_Bad( string com_port )
            : base( com_port )
        {

        }
        /*************************************************************************/
        public override void GatherDeviceDetails()
        {
            base.GatherDeviceDetails();
            DeviceName = "0490";
            DeviceType = Type.VDA;
        }

        /*************************************************************************/
        private int _c = 0;
        public override string CmdDoAnalysis()
        {
            base.CmdDoAnalysis();
            return ( _c++ % 2 == 0 ) ? "00005 47474" : "99999 47478";
        }

        /*************************************************************************/
        public override string ToString()
        {
            return "TEST VDA OBJECT (BAD) " + "[ Name: " + DeviceName + " SRev: " + SoftwareRevision + " SerNo: " + SerialNumber + " Firm: " + FirmwareBuild + "]";
        }
    }
} ;
