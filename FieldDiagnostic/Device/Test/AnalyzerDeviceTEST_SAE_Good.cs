﻿using System.Collections.Generic;

namespace FieldDiagnostic.Device.Test
{
    public class AnalyzerDeviceTEST_SAE_Good : AnalyzerDeviceTEST
    {
        /*************************************************************************/
        // ReSharper disable once UnusedParameter.Local
        public AnalyzerDeviceTEST_SAE_Good( string com_port )
            : base( "DEBUG" )
        {
        }

        /*************************************************************************/
        public override void GatherDeviceDetails()
        {
            base.GatherDeviceDetails();
            DeviceName = "0520";
            DeviceType = Type.SAE;
        }

        /*************************************************************************/
        public override List<string> CmdGetLastResults()
        {
            var results = base.CmdGetLastResults();

            // Sloppy, but this is DebugWorld™.
            results.Clear();

            const int K = 47;
            for ( int i = 0; i < 5; i++ ) {
                string code = ( i % 2 == 0 ) ?
                                    "003.0 098.0 001.5 001.0 000.0 00000 000" :
                                    "003.0 000.0 001.5 001.0 100.0 00005 000";

                results.Add( code + " 000" + ( K + i ).ToString() );
            }

            return results;
        }

        /*************************************************************************/
        private int _c = 0;
        public override string CmdDoAnalysis()
        {
            base.CmdDoAnalysis();
            return ( _c++ % 2 == 0 ) ? "003.0 100.0 001.5 001.0 000.0 00000 02527" : // gas
                                       "003.0 000.0 001.5 001.0 100.0 00005 02528";  // air
        }

        /*************************************************************************/
        public override string ToString()
        {
            return "TEST SAE OBJECT (GOOD) " + "[ Name: " + DeviceName + " SRev: " + SoftwareRevision + " SerNo: " + SerialNumber + " Firm: " + FirmwareBuild + "] on " + PortName;
        }
    }
}
