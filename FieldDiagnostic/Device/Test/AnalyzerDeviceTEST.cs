﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace FieldDiagnostic.Device.Test
{
    public abstract class AnalyzerDeviceTEST : AnalyzerDevice
    {
        public AnalyzerDeviceTEST( string com_port )
            : base( "DEBUG" )
        {
            Debug.WriteLine( "* Opening pretend serial port" );
            GatherDeviceDetails();
        }

        /*************************************************************************/
        public override void Done()
        {
            Debug.WriteLine( "* Closing pretend serial port" );
        }

        /*************************************************************************/
        public override int CmdSystemCheck()
        {
            Debug.WriteLine( "* Pretending to do system check" );
            return Response.ACK;
        }

        /*************************************************************************/
        public override int CmdResetFilterCounter()
        {
            Debug.WriteLine( "* Pretending to reset filter counter" );
            return Response.ACK;
        }

        /*************************************************************************/
        public override void GatherDeviceDetails()
        {
            Debug.WriteLine( "* Pretending to gather device details" );

            DeviceName = "0490";
            DeviceType = Type.VDA;
            SoftwareRevision = "0147";

            Random rand = new Random();
            SerialNumber = rand.Next( 1000000, 9999999 ).ToString();
            FirmwareBuild = rand.Next( 1000, 9999 ) + "-" + rand.Next( 1000, 9999 ) + "-" + rand.Next( 1000, 9999 );

            LastResults = CmdGetLastResults();
        }

        /*************************************************************************/
        public override List<string> CmdGetLastResults()
        {
            Debug.WriteLine( "* Pretending to get last results" );
            List<string> results = new List<string>( 5 );

            const int K = 47;
            for ( int i = 0; i < 5; i++ ) {
                string code = ( i % 2 == 0 ) ? "00000" : "00005";
                results.Add( code + " 000" + ( K + i ).ToString() );
            }

            return results;
        }

        public override void CmdDoCalibration()
        {
            Debug.WriteLine( "* Pretending to do calibration" );
        }

        //private int c = 1;
        //public override string CmdDoAnalysis()
        //{
        //    Debug.WriteLine( "* Pretending to run analysis" );
        //    c++;
        //    return ( c % 2 == 0 ) ? "00005 47474" : "99999 47474";
        //}

        public override string CmdDoAnalysis()
        {
            Debug.WriteLine( "* Pretending to run analysis" );
            return null;
        }

        /*************************************************************************/
        public override string ToString()
        {
            //return "[ Name: " + DeviceName + " SRev: " + SoftwareRevision + " SerNo: " + SerialNumber + " ] on " + PortName;
            return "TEST VDA OBJECT";
        }
    }
}
